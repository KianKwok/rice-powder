#ifndef POS_H
#define POS_H


class Pos {
    friend class Snake;
    friend class Food;
public:
    Pos(int x = 0, int y = 0);
    bool operator==(const Pos &another);
    Pos operator+(const Pos &another);
    Pos operator-(const Pos &another);
protected:
    int x_;
    int y_;
};

#endif // POS_H
