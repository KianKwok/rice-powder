#ifndef CELL_H
#define CELL_H


typedef enum _CellStatus {
    BLANK, MINE, DIGIT
} CellStatus;

class Cell {
public:
    Cell();
    void setMine(int mine);
    int getMine();
    void setStatus(CellStatus status);
    CellStatus getStatus();

protected:
    int mine_;
    CellStatus status_;

};

#endif // CELL_H
