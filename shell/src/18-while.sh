#########################################################################
# File Name: 18-while.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Sat Feb 15 00:02:27 2020
#########################################################################
#!/bin/bash

count=0
while [ ${count} -lt 5 ]
do
	count=$((${count} + 1))
	echo ${count}
done

echo ‐n 'input film: '
while read FILM
do
	echo "great film the $FILM"
done
