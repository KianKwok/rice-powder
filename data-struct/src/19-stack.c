#include <stdio.h>
#include <stdlib.h>

typedef struct _Stack {
    int len_;
    int top_;
    char *space_;
} Stack;

void initStack(Stack *s, int size);
int isFullStack(Stack *s);
int isEmptyStack(Stack *s);
void pushStack(Stack *s, char data);
char popStack(Stack *s);
void resetStack(Stack *s);
void destoryStack(Stack *s);

int main() {
    Stack s;
    printf("Hello World!\n");
    initStack(&s, 100);

    for (char ch = 'A'; ch <= 'Z'; ++ch) {
        if (!isFullStack(&s))
            pushStack(&s, ch);
    }
    while (!isEmptyStack(&s)) {
        printf("%c ", popStack(&s));
    }
    putchar(10);
    destoryStack(&s);
    printf("Hello World!\n");
    return 0;
}
void initStack(Stack *s, int size) {
    s->len_ = size;
    s->top_ = 0;
    s->space_ = (char *)malloc(sizeof(char));
}
int isFullStack(Stack *s) {
    return (s->top_ == s->len_);
}
int isEmptyStack(Stack *s) {
    return (s->top_ == 0);
}
void pushStack(Stack *s, char data) {
    s->space_[s->top_++] = data;
}
char popStack(Stack *s) {
    return s->space_[--s->top_];
}
void resetStack(Stack *s) {
    s->top_ = 0;
}
void destoryStack(Stack *s) {
    free(s->space_);
}
