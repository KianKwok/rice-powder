#include <iostream>
#include <vector>

using namespace std;

class Wing {
public:
    Wing() {
        cout << "Wing created" << endl;
    }
    ~Wing() {
        cout << "Wing destoried" << endl;
    }
};
class Bird {
public:
    Bird() {
        left_ = new Wing;
        right_ = new Wing;
    }
    ~Bird() {
        delete left_;
        delete right_;
    }
private:
    // composition
    Wing *left_;
    Wing *right_;
};
class Flock {
public:
    void flyIn(Bird *b) {
        vb.push_back(b);
    }
    Bird *flyOut() {
        Bird *temp = vb.back();
        vb.pop_back();
        return temp;
    }
private:
    // aggregation
    vector<Bird *> vb;
};

int main() {
    Bird *b1 = new Bird;
    Bird *b2 = new Bird;
    Bird *b3 = new Bird;
    Flock f;
    f.flyIn(b1);
    f.flyIn(b2);
    f.flyIn(b3);
    cout << "Hello World!" << endl;
    return 0;
}
