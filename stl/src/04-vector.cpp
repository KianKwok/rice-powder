#include <iostream>
#include <vector>

using namespace std;

void vi_constructor() {
    vector<int> v1;        // empty
    vector<int> v2(5);     // v2 has 5 elems
    vector<int> v3(6, 7);  // v3 has 6 elems, every elem is 7
    vector<int> v4 = {1,2,3,4,5,6};
    vector<int> v5(v3);    // v5 is the same to v3
}
void vi_assignment() {
    vector<int> vi1;
    vi1 = {1,2,3,4,5};

    vector<int> vi2;
    vi2 = vi1;

    vector<int> vi3;
    vi3.assign(5,1);      // vi3 has 5 elems, each elem is 1

    vector<int> vi4 = {1,2,3,4};
    vi4.assign(5,2);      // the original vi4 has been deleted, the new vi4 has 5 elems, each elem is 2
    for (auto &i:vi4)
        cout << i << " ";
    putchar(10);

    vector<int> vi5;
    vi5.assign(vi1.begin(), vi1.end());

    vector<int> vi6;
    vi6.assign({1,3,5,7,9});

    // vi5.swap(vi6) is better than std::swap(vi5, vi6)
    vi5.swap(vi6);
    std::swap(vi5, vi6);
}
void vi_index() {
    vector<int> vi = {1,2,3,4,5,6,7};
    cout << "vi[0]    = " << vi[0] << endl;
    cout << "vi.at(1) = " << vi.at(1) << endl;
    cout << "front    = " << vi.front() << endl;
    for (int i = 0; i < vi.size(); ++i)
        cout << vi[i] << endl;
    putchar(10);
}
void vi_front_back() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};
    cout << "front = " << vi.front() << endl;
    cout << "back  = " << vi.back() << endl;
    vi.push_back(100);
    cout << "front = " << vi.front() << endl;
    cout << "back  = " << vi.back() << endl;
    vi.pop_back();
    cout << "front = " << vi.front() << endl;
    cout << "back  = " << vi.back() << endl;
}
void vi_traversal() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
}
void vi_insert() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);

    vi.insert(vi.begin(), 999);
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
    vi.insert(vi.end(), 888);
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
}
void vi_erase() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);

    vi.erase(vi.begin());
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
    cout << "+++++++++" << endl;
//    vi.erase(vi.end()); // this is a error code.
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
}
int main() {
//    vi_constructor();
//    vi_assignment();
    vi_index();
//    vi_front_back();
//    vi_traversal();
//    vi_insert();
//    vi_erase();
    cout << "Hello World!" << endl;
    return 0;
}
