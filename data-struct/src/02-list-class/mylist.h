#ifndef MYLIST_H
#define MYLIST_H


typedef struct _Node {
    int data_;
    struct _Node *next_;
} Node;

class MyList
{
public:
    MyList();
    ~MyList();
    void traversal();
    int len();
    void insert(int data);
    Node * search(int key);
    void erase(Node *pfind);
    void sort();
    void invert();

private:
    int len_;
    Node *head_;
};

#endif // MYLIST_H
