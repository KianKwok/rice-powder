#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void printInt(int &n) {
    cout << n << " ";
}

int main() {
    vector<int> vi;
    for (int i = 0; i < 5; ++i)
        vi.push_back(i);
    sort(vi.begin(), vi.end(), greater<int>());
    sort(vi.begin(), vi.end(), less_equal<int>());
    stable_sort(vi.begin(), vi.end(), greater_equal<int>());

    for_each(vi.begin(), vi.end(), printInt);
    putchar(10);
    cout << "Hello World!" << endl;
    return 0;
}
