#ifndef TOKYOTIME_H
#define TOKYOTIME_H


#include "observer.h"

class TokyoTime : public Observer {
public:
    TokyoTime();
    void uplate(int hour, int min, int sec);
};

#endif // TOKYOTIME_H
