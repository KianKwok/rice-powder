# XML & JSON

XML

* [tinyxml write](./src/01-tinyxml-write/)
* [tinyxml read](./src/02-tinyxml-read/)

JSON

* [jsoncpp write](./src/03-jsoncpp-write/)
* [jsoncpp read](./src/04-jsoncpp-read/)