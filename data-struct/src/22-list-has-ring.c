#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
    int data_;
    struct _Node *next_;
} Node;

Node * createList(void);
void traversalList(Node *head);
void insertList(Node *head, int data);
void destoryList(Node *head);

int hasRing(Node *head) {
    if (NULL == head)
        return -1;
    if (NULL == head->next_ || NULL == head->next_->next_)
        return 0;
    Node *temp1 = head->next_;
    Node *temp2 = head->next_->next_;
    while (temp2->next_ != NULL) {
        if (temp1 == temp2)
            return 1;
        temp1 = temp1->next_;
        if (NULL == temp2->next_)
            return 0;
        temp2 = temp2->next_->next_;
    }
    return 0;
}

int main() {
    Node *head = createList();

    for (int i = 0; i < 10; ++i) {
        insertList(head, i);
    }

    printf("%d\n", hasRing(head));
    destoryList(head);
    printf("Hello World!\n");
    return 0;
}

Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->next_ = NULL;
    return head;
}
void traversalList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        printf("%d ", head->next_->data_);
        head = head->next_;
    }
    putchar(10);
}
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;
    temp->next_ = head->next_;
    head->next_ = temp;
}
void destoryList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        Node *temp = head->next_;
        free(head);
        head = temp;
    }
}
