#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAXROW 10
#define MAXCOL 10

typedef struct _Point {
    int x_;
    int y_;
} Point;
typedef struct _Node {
    Point data;
    struct _Node *next;
} Node;
typedef struct _Stack {
    Node *top;
} Stack;

void initStack(Stack *s);
int isEmptyStack(Stack *s);
void pushStack(Stack *s, Point data);
Point popStack(Stack *s);
void resetStack(Stack *s);
void clearStack(Stack *s);

void displayMaze(int maze[][MAXCOL]);

Point prePoint[MAXROW][MAXCOL];

void visit(Stack *s, int x, int y, Point t) {
    Point p = {x, y};
    pushStack(s, p);

    prePoint[x][y] = t;
}

void dispalyPoint() {
    for (int i = 0; i < MAXROW; ++i) {
        for (int j = 0; j < MAXCOL; ++j) {
            printf("(%2d,%2d)", prePoint[i][j].x_, prePoint[i][j].y_);
        }
        putchar(10);
    }
    for (int i = 0; i < 70; ++i) {
        putchar('=');
    }
    putchar(10);
}

int main() {
    int maze[MAXROW][MAXCOL] = {
        {1,1,1,1,1,1,1,1,1,1},
        {0,0,0,1,1,1,1,1,1,1},
        {1,1,0,1,1,1,1,1,1,1},
        {1,1,0,0,0,1,1,1,1,1},
        {1,1,0,1,1,0,1,1,1,1},
        {1,1,0,0,0,0,1,1,1,1},
        {1,1,1,1,1,0,1,1,1,1},
        {1,1,1,1,1,0,0,0,1,1},
        {1,1,1,1,1,1,1,0,0,0},
        {1,1,1,1,1,1,1,1,1,1},
    };
    Stack s;

    Point sp = {1, 0};
    Point ep = {8, 9};

    memset(prePoint, 0xff, sizeof(Point) * MAXROW * MAXCOL);
    dispalyPoint();
#if 1
    initStack(&s);
    pushStack(&s, sp);

    int flag = 1;

    while (!isEmptyStack(&s)) {
        Point t;
        t = popStack(&s);
        maze[t.x_][t.y_] = 2;

        system("cls");
        displayMaze(maze);
        sleep(1);

        if (t.x_ - 1 >= 0 && maze[t.x_ - 1][t.y_] != 1 && maze[t.x_ - 1][t.y_] != 2) {
            visit(&s, t.x_ - 1, t.y_, t);
        }
        if (t.x_ + 1 < MAXROW && maze[t.x_ + 1][t.y_] != 1 && maze[t.x_ + 1][t.y_] != 2) {
            visit(&s, t.x_ + 1, t.y_, t);
        }
        if (t.y_ - 1 >= 0 && maze[t.x_][t.y_ - 1] != 1 && maze[t.x_][t.y_ - 1] != 2) {
            visit(&s, t.x_, t.y_ - 1, t);
        }
        if (t.y_ + 1 < MAXCOL && maze[t.x_][t.y_ + 1] != 1 && maze[t.x_][t.y_ + 1] != 2) {
            visit(&s, t.x_, t.y_ + 1, t);
        }

        if (t.x_ == ep.x_ && t.y_ == ep.y_) {
            flag = 0;
            break;
        }
    }
    if (flag == 0)
        printf("find path!\n");
    else
        printf("No path!\n");
#endif
//    dispalyPoint();

    Point t = ep;
    while (t.y_ != -1) {
        printf("(%d,%d)", t.x_, t.y_);
        t = prePoint[t.x_][t.y_];
    }
    putchar(10);
    printf("Hello World!\n");
    return 0;
}
void displayMaze(int maze[][MAXCOL]) {
    for (int i = 0; i < MAXROW; ++i) {
        for (int j = 0; j < MAXCOL; ++j) {
            if (maze[i][j] == 1)
                printf("%2s", " *");
            else if (maze[i][j] == 2)
                printf("%2s", " #");
            else
                printf("%2s", "  ");
        }
        putchar(10);
    }
    printf("====================\n");
}
void initStack(Stack *s) {
    s->top = (Node *)malloc(sizeof(Node));
    s->top->next = NULL;
}
int isEmptyStack(Stack *s) {
    return (s->top->next == NULL);
}
void pushStack(Stack *s, Point data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    temp->data = data;
    temp->next = s->top->next;
    s->top->next = temp;
}
Point popStack(Stack *s) {
    Node *temp = s->top->next;
    Point ch = temp->data;
    s->top->next = temp->next;
    free(temp);
    return ch;
}
void resetStack(Stack *s) {
    while (!isEmptyStack(s)) {
        popStack(s);
    }
}
void clearStack(Stack *s) {
    resetStack(s);
    free(s->top);
}
