#include "subject.h"

Subject::Subject() {

}
void Subject::registerObserver(Observer *ob) {
    lo_.push_back(ob);
}
void Subject::removeObserver(Observer *ob) {
    lo_.remove(ob);
}
