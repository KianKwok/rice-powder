#ifndef FOOD_H
#define FOOD_H


#include "Pos.h"

class Snake;

class Food {
public:
    Food(Snake &s);
    void setFood();
    Pos getPos();
    bool isFood(const Pos &p);
protected:
    Pos pos_;
    Snake &snake_;
};

#endif // FOOD_H
