#include <iostream>
#include <vector> #include <algorithm>

using namespace std;

class Compare {
public:
    bool operator() (int i, int j) {
        return i > j;
    }
};

int main() {
    int arr[10] = {1,3,5,7,9,2,4,6,8,10};
    vector<int, allocator<int>> vi;
    vi.assign(arr, arr + 10);

    int *p;
    for (p = arr; p != arr + 10; ++p) {
        cout << *p << ' ';
    }
    putchar(10);

    // itr is a intelligent pointer
    vector<int, allocator<int>>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ++itr )
        cout << *itr << ' ';
    putchar(10);

    sort(vi.begin(), vi.end(), Compare());

    for (auto &i:vi)
        cout << i << ' ';
    putchar(10);

    cout << "Hello World!" << endl;
    return 0;
}
