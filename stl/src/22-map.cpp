#include <iostream>
#include <map>

using namespace std;

void test_basic() {
#if 0
    map<int, string> mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "fff")
    };
#elif 0
    map<int, string, less<int>> mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "fff")
    };
#else
    map<int, string, greater<int>> mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "fff")
    };
#endif
    cout << "size : " << mis.size() << endl;
    mis.insert(pair<int, string> (5, "ggg")); // key repeat, insert fail
    mis.insert(map<int, string>::value_type (6, "hhh"));
    mis.insert(make_pair<int, string> (7, "iii"));

    mis[5] = "xxx";   // modify pair with the key
    mis[100] = "yyy"; // insert new pair

    map<int, string>::iterator itr;
    for (itr = mis.begin(); itr != mis.end(); ++itr) {
//        cout << itr->first << ":" << itr->second << endl;
        cout << (*itr).first << ":" << (*itr).second << endl;
    }
}
void test_multimap() {
    multimap<int, string> mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "fff")
    };
    mis.insert(pair<int, string> (5, "ggg")); // insert success
//    mis[5] = "hhh"; // error
//    mis[1] = "xxx"; // error
    for (auto itr = mis.begin(); itr != mis.end(); ++itr) {
        cout << (*itr).first << ":" << (*itr).second << endl;
    }
}
void test_insert() {
    map<int, string> mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "fff")
    };
    mis.insert(mis.find(100), pair<int, string>(100, "eee"));
    mis.insert(mis.find(5), pair<int, string>(200, "ggg"));

    mis.erase(100);

    for (auto itr = mis.begin(); itr != mis.end(); ++itr) {
        cout << (*itr).first << ":" << (*itr).second << endl;
    }
}
void test_erase() {
    map<int, string> mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "fff")
    };
    mis.insert(pair<int, string>(100, "eee"));
    mis.insert(pair<int, string>(100, "ggg"));
    mis.insert(pair<int, string>(100, "hhh"));
    mis.insert(pair<int, string>(100, "iii"));
    mis.insert(pair<int, string>(100, "jjj"));
    mis.insert(pair<int, string>(100, "kkk"));

    for (auto itr = mis.begin(); itr != mis.end(); ++itr) {
        cout << (*itr).first << ":" << (*itr).second << " ";
    }
    putchar(10);

//    mis.erase(100);
//    mis.erase(mis.begin());
    auto it = mis.find(2);
    if (it != mis.end())
        mis.erase(it);

//    mis.clear();

    for (auto itr = mis.begin(); itr != mis.end(); ++itr) {
        cout << (*itr).first << ":" << (*itr).second << " ";
    }
    putchar(10);
}
void test_bound() {
    map<int, string> mis;
    mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "eee"),
        pair<int, string> (6, "fff"),
        pair<int, string> (7, "ggg"),
        pair<int, string> (8, "hhh"),
        pair<int, string> (9, "iii"),
        pair<int, string> (10,"jjj")
    };

    auto low_itr = mis.lower_bound(5);
    auto upp_itr = mis.upper_bound(7);

    cout << "low : " << low_itr->first << endl;
    cout << "upp : " << upp_itr->first << endl;

    for (auto itr = low_itr; itr != upp_itr;) {
        itr = mis.erase(itr);
    }

    // delete --- you'd better don't using traversal
    mis.erase(low_itr, upp_itr);
}
void test_equal_range() {
    multimap<int, string> mis;
    mis = {
        pair<int, string> (1, "aaa"),
        pair<int, string> (2, "bbb"),
        pair<int, string> (3, "ccc"),
        pair<int, string> (4, "ddd"),
        pair<int, string> (5, "a"),
        pair<int, string> (5, "b"),
        pair<int, string> (5, "c"),
        pair<int, string> (5, "d"),
        pair<int, string> (6, "fff"),
        pair<int, string> (7, "ggg"),
        pair<int, string> (8, "hhh"),
        pair<int, string> (9, "iii"),
        pair<int, string> (10,"jjj")
    };

    auto range1 = mis.equal_range(5);
    pair<multimap<int, string>::iterator, multimap<int, string>::iterator> range2 = mis.equal_range(5);

    cout << "low : " << range1.first->first << ", " << range1.first->second << endl;
    cout << "upp : " << range1.second->first<< ", " << range1.second->second<< endl;
}


int main() {
//    test_basic();
//    test_multimap();
//    test_insert();
    test_erase();
    test_bound();
    test_equal_range();
    cout << "Hello World!" << endl;
    return 0;
}
