#include <time.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
    int data;
    struct _Node *next;
    struct _Node *pre;
} Node;
Node * createList(void);
void insertList(Node *head, int data);
void traverseList1(Node *head);
void traverseList2(Node *head);

int main() {
    Node *head = createList();
    srand((unsigned int)time(NULL));
    for (int i = 0; i < 10; ++i) {
        insertList(head, rand() % 100);
    }
    traverseList1(head);
    printf("Hello World!\n");
    return 0;
}

Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->pre = head;
    head->next = head;
    return head;
}
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data = data;
    temp->next = head->next;
    temp->pre = head;
    head->next = temp;
    temp->next->pre = temp;
}
void traverseList1(Node *head) {
    Node *temp = head->next;
    while (temp != head) {
        printf("%2d ", temp->data);
        temp = temp->next;
    }
    putchar(10);
}
void traverseList2(Node *head) {
    Node *temp = head->pre;
    while (temp != head) {
        printf("%2d ", temp->data);
        temp = temp->pre;
    }
    putchar(10);
}
