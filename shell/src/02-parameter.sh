#########################################################################
# File Name: 02-parameter.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Wed Feb 12 20:58:15 2020
#########################################################################
#!/bin/bash

echo "param 0 : ${0}"
echo "param 1 : ${1}"
echo "param 2 : ${2}"
echo "numb of param : $#"

echo "all param : $*"
echo "all param : $@"
echo "return : $?"

echo "process ID : $$"
