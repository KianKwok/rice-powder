#include "Screen.h"
#include "Snake.h"
#include "Food.h"
#include <iostream>

using std::cout;

extern int weight;
extern int height;

Screen::Screen() {

}

void Screen::screenFlush(Snake *s, Food *f) {
    for (int i = 0; i < weight; ++i) {
        for (int j = 0; j < height; ++j) {
            if (i == 0 || i == weight - 1 || j == 0 || j == height - 1) {
                cout << " #";
            } else if (s->isSnakeBody(Pos(i, j))) {
                cout << " @";
            } else if (f->isFood(Pos(i, j))) {
                cout << " *";
            } else {
                cout << "  ";
            }
        }
        putchar(10);
    }
}
