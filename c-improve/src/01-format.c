#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// The data type is the formatting of memory.

typedef struct _Stuff {
    char name_[4];
    int score_;
} Stuff;
typedef union _Un {
    char name_[8];
    int score_;
} Un;

void SignedAndUnsigned(void);
void Pointer(void);

int main() {
//    SignedAndUnsigned();
    Pointer();
    printf("Hello World!\n");
    return 0;
}
void SignedAndUnsigned(void) {
    signed char ch1 = (signed char)0xff;
    printf("%d\n", ch1);

    unsigned char ch2 = 0xff;
    printf("%u\n", ch2);
}
void Pointer(void) {
//    char p[8];
    void *p = malloc(8); // 8 Bytes

    char *cp = (char *)p;
    for (int i = 0; i < 8; ++i) {
        cp[i] = (char)('a' + i);
    }
    for (int i = 0; i < 8; ++i) {
        printf("%c ", cp[i]);
    }
    putchar(10);

    short *sp = (short *)p;
    for (int i = 0; i < 4; ++i) {
        sp[i] = (short)(100 + i);
    }
    for (int i = 0; i < 4; ++i) {
        printf("%d ", sp[i]);
    }
    putchar(10);

    double *dp = (double *)p;
    *dp = 1.2345;
    printf("*dp = %f\n", *dp);

    char *csp = (char *)p;
    strcpy(csp, "abcde");
    printf("csp = %s\n", csp);

    Stuff *stp = (Stuff *)p;
    strcpy(stp->name_, "ab");
    stp->score_ = 99;
    printf("Struct name = %s, score = %d\n", stp->name_, stp->score_);

    Un *pu = (Un *)p;
    strcpy(pu->name_, "ab");
    // The data members in union have sequential validity.
    printf("Union1 name = %s, score = %d\n", pu->name_, pu->score_);
    pu->score_ = 88;
    printf("Union2 name = %s, score = %d\n", pu->name_, pu->score_);

    free(p);
}
