#include <iostream>

using namespace std;

class Phone {
public:
    virtual int cost() = 0;
};
class Nokia : public Phone {
public:
    int cost() {
        return 5000;
    }
};
class DecoratePhone : public Phone {
public:
    DecoratePhone(Phone *ph)
        :phone_(ph) {}
protected:
    Phone *phone_;
};
class ScreeProtector : public DecoratePhone {
public:
    ScreeProtector(Phone *ph)
        :DecoratePhone(ph) {}
    int cost() {
        return 100 + phone_->cost();
    }
};
class HeadSetPhone : public DecoratePhone {
public:
    HeadSetPhone(Phone *ph)
        :DecoratePhone(ph) {}
    int cost() {
        return 199 + phone_->cost();
    }
};

int main() {
    Nokia nk;
    cout << nk.cost() << endl;
    ScreeProtector sp(&nk);
    cout << sp.cost() << endl;

    HeadSetPhone hp(&sp);
    cout << hp.cost() << endl;

    Phone *p = new HeadSetPhone(new HeadSetPhone(new ScreeProtector(new ScreeProtector(new Nokia))));
    cout << p->cost() << endl;
    cout << "Hello World!" << endl;
    return 0;
}
