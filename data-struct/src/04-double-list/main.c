#include "doublelist.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    Node *head = createList();

    srand((unsigned int)time(NULL));
    for (int i = 0; i < 10; ++i) {
        insertList(head, rand() % 10);
    }
    traversalList(head);

    sortList(head);
    traversalList(head);
    printf("Hello World!\n");
    return 0;
}
