#ifndef DOUBLELIST_H
#define DOUBLELIST_H


typedef struct _Node {
    int data_;
    struct _Node *next_;
    struct _Node *pre_;
} Node;

Node * createList(void);
void insertList(Node *head, int data);
void traversalList(Node *head);
Node * searchList(Node *head, int key);
void eraseList(Node *head, Node *pfind);
int lenList(Node *head);

// 1. swap data
// 2. swap pointer
void sortList(Node *head);
void destoryList(Node *head);

#endif // DOUBLELIST_H
