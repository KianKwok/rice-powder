#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
    int data_;
    struct _Node *next_;
} Node;

Node * createList(void);
void traversalList(Node *head);
void insertList(Node *head, int data);
void eraseList(Node *head, int key);
void destoryList(Node *head);

Node * josephus(Node *head, int m) {
    if (NULL == head)
        return NULL;
    Node *thead = head;
    if (thead->next_ == head)
        return NULL;
    int num = 1;
    while (1) {
        if (thead->next_->next_ == thead)
            break;
        printf("num = %2d, data = %d\n", num, thead->next_->data_);
        if (num % m == 0) {
            Node *temp = thead->next_;
            thead->next_ = temp->next_;
            printf("kill node : %d\n", temp->data_);
            free(temp);
        } else {
            thead = thead->next_;
        }
        if (thead->next_ == head)
            thead = thead->next_;
        ++num;
    }
    printf("survival node : %d\n", thead->next_->data_);
    return thead->next_;
}

int main() {
    Node *head = createList();

    int n = 6;
    int m = 5; // kill the m_th node

    for (int i = 0; i < n; ++i) {
        insertList(head, i + 1);
    }
    traversalList(head);

    josephus(head, m);
    traversalList(head);

    destoryList(head);
    printf("Hello World!\n");
    return 0;
}

Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->next_ = head;
    return head;
}
void traversalList(Node *head) {
    if (NULL == head)
        return ;
    Node *thead = head;
    while (thead->next_ != head) {
        printf("%d ", thead->next_->data_);
        thead = thead->next_;
    }
    putchar(10);
}
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;
    temp->next_ = head->next_;
    head->next_ = temp;
}
void eraseList(Node *head, int key) {
    if (NULL == head)
        return ;
    Node *thead = head;
    while (thead->next_ != head) {
        if (thead->next_->data_ == key) {
            Node *temp = thead->next_;
            thead->next_ = temp->next_;
            free(temp);
        }
        thead = thead->next_;
    }
}
void destoryList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != head) {
        Node *temp = head->next_;
        head->next_ = temp->next_;
        free(temp);
    }
    free(head);
}
