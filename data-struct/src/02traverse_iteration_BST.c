#include <stdio.h>
#include <stdlib.h>

typedef struct _TreeNode {
    struct _TreeNode *right_;
    struct _TreeNode *left_;
    int data_;
} TreeNode;

typedef struct _Stack {
    int len_;
    int top_;
    TreeNode **space_;
} Stack;

void initStack(Stack *s, int size);
int isStackEmpty(Stack *s);
void push(Stack *s, TreeNode *ch);
TreeNode * pop(Stack *s);

void PreOrder(TreeNode *r) {
    if (r) {
        Stack s;
        initStack(&s, 1000);
        while (r || !isStackEmpty(&s)) {
            while (r) {
                printf("%d ", r->data_);
				push(&s, r);
				r = r->left_;
            }
			r = pop(&s);
			r = r->right_;
        }
    }
}
void MidOrder(TreeNode *r) {
    if (r) {
        Stack s;
        initStack(&s, 1000);
        while (r || !isStackEmpty(&s)) {
            while (r) {
                push(&s, r);
                r = r->left_;
            }
            r = pop(&s);
            printf("%d ", r->data_);
            r = r->right_;
        }
    }
}
void PostOrder(TreeNode *r) {
    Stack s;
    initStack(&s, 1000);
    TreeNode *cur;
    TreeNode *pre = NULL;
    push(&s, r);
    while (!isStackEmpty(&s)) {
        cur = pop(&s);
        push(&s, cur);
        if (((cur->left_ == NULL) && (cur->right_ == NULL))
                || (pre != NULL && (pre == cur->left_ || pre == cur->right_))) {
            printf("%d ", cur->data_);
            pop(&s);
            pre = cur;
        } else {
            if (cur->right_ != NULL)
                push(&s, cur->right_);
            if (cur->left_ != NULL)
                push(&s, cur->left_);
        }
    }
}

int main() {
    TreeNode a, b, c, d, e, f;
    TreeNode *root = &a;

    a.data_ = 1;
    b.data_ = 2;
    c.data_ = 3;
    d.data_ = 4;
    e.data_ = 5;
    f.data_ = 6;

    a.left_ = &b; a.right_ = &c;
    b.left_ = &d; b.right_ = &e;
    c.left_ = c.right_ = NULL;
    d.left_ = d.right_ = NULL;
    e.left_ = &f; e.right_ = NULL;
    f.left_ = f.right_ = NULL;

    PreOrder(root);
    putchar(10);
    MidOrder(root);
    putchar(10);
    PostOrder(root);
    putchar(10);
    printf("Hello World!\n");
    return 0;
}

void initStack(Stack *s, int size) {
    s->top_ = 0;
    s->len_ = size;
    s->space_ = (TreeNode **)malloc(sizeof(TreeNode *) * (unsigned long long)s->len_);
}
void push(Stack *s, TreeNode *ch) {
    s->space_[s->top_++] = ch;
}
TreeNode * pop(Stack *s) {
    return s->space_[--s->top_];
}
int isStackEmpty(Stack *s) {
    return s->top_ == 0;
}
