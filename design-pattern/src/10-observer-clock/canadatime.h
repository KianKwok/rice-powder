#ifndef CANADATIME_H
#define CANADATIME_H


#include "observer.h"

class CanadaTime : public Observer {
public:
    CanadaTime();
    void uplate(int hour, int min, int sec);
};

#endif // CANADATIME_H
