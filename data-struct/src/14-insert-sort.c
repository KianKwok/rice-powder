#include <stdio.h>

void InsertSort(int *arr, int len) {
    int i, j ,t;
    for (i = 1; i < len; ++i) {
        t = arr[i];
        for (j = i; j - 1 >= 0 && t < arr[j - 1]; --j) {
            arr[j] = arr[j - 1];
        }
        arr[j] = t;
    }
}
void display(int *arr, int len) {
    for (int i = 0; i < len; ++i) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int arr[] = {1,3,5,7,9,2,4,6,8};
    InsertSort(arr, sizeof(arr)/sizeof(*arr));
    display(arr, sizeof(arr)/sizeof(*arr));
    putchar(10);

    printf("Hello World!\n");
    return 0;
}
