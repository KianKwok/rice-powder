#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9};

    random_shuffle(vi.begin(), vi.end());

    for (auto &i:vi)
        cout << i << " ";
    putchar(10);
    cout << "Hello World!" << endl;
    return 0;
}
