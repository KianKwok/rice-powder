#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void test1() {
    int myints[] = {1,2,3,3,2,1,1,2};
    int *pbegin = myints;
    int *pend = myints + sizeof(myints) / sizeof(int);

    pend = remove(pbegin, pend, 2);

    for (int *p = pbegin; p != pend; ++p)
        cout << *p << " ";
    putchar(10);
}
void test2() {
    string str1 = "Text with some spaces";
    str1.erase(remove(str1.begin(), str1.end(), ' '), str1.end());
    cout << str1 << endl;
    string str2 = "Text\n with\n some\n spaces\n";
    str2.erase(remove_if(str2.begin(), str2.end(), [] (char x) {return isspace(x);}), str2.end());
    cout << str2 << endl;
}
int test3() {
    vector<int> vi = {1,2,3,3,2,1,1,2};
    cout << vi.size() << endl; // 8
    auto enditr = remove(vi.begin(), vi.end(), 3);
    cout << vi.size() << endl; // 8

    for (auto i = vi.begin(); i != enditr; ++i)
        cout << *i << " ";
    putchar(10);

    vi.erase(enditr, vi.end());

    for (auto i = vi.begin(); i != vi.end(); ++i)
        cout << *i << " ";
    putchar(10);
}

int main() {
    cout << "Hello World!" << endl;
    return 0;
}
