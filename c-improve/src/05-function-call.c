#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *fa() {
    char *pa = "123456"; //       pa : stack
                         // "123456" : constants area
                                     // the pointer variable pa is released after the function is called
    char *p = NULL;                  // pointer p alloc 4 bytes in stack
    p = (char*)malloc(100);          // alloc 100 bytes memory space in heap
                                     // assign the address to p
    strcpy(p, "wudunxiong 1234566"); // copy the string from constants area to heap
    return p;                        // return to main tuning function fb();
}
char *fb() {
    char *pstr = NULL;
    pstr = fa();
    return pstr;             // the pointer pstr disappeared here.
}
                             // for fb(), fa() is called function.
                             // for main(), both fa() and fb() are called function.
int main() {
    char *str = NULL;
    str = fb();              // the value of memory allocated by the tuning function is passed to the main tuning function by returning value,
                             // the main tuning function frees the memory
    printf("str = %s\n",str);
    free(str);               // prevent memory leakage.
    str = NULL;              // prevents the generation of wild pointers
    printf("Hello World!\n");
    return 0;
}
