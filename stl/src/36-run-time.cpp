#include <iostream>
#include <vector>
#include <time.h>
#include <stdlib.h>

using namespace std;

int main() {
    srand(time(nullptr));

    vector<string> vs;

    long start_time = clock();

    char buf[1024];

    for (int i = 0; i < 1000000; ++i) {
        snprintf(buf, 10, "%d", rand() % 1000000);
        vs.push_back(string(buf));
    }

    cout << "run time (ms) : " << clock() - start_time << endl;
    cout << "Hello World!" << endl;
    return 0;
}
