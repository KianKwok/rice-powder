#include "mylist.h"
#include <iostream>

using std::cout;

MyList::MyList() {
    head_ = new Node;
    head_->next_ = nullptr;
    len_ = 0;
}
MyList::~MyList() {
    while (head_->next_ != nullptr) {
        Node *temp = head_->next_;
        head_->next_ = temp->next_;
        delete temp;
    }
    delete head_;
}
void MyList::traversal() {
    Node *tHead = head_;
    while (tHead->next_ != nullptr) {
        cout << tHead->next_->data_ << " ";
        tHead = tHead->next_;
    }
    putchar(10);
}
int MyList::len() {
    return len_;
}
void MyList::insert(int data) {
    Node *temp = new Node;
    temp->data_ = data;
    temp->next_ = head_->next_;
    head_->next_ = temp;
    ++len_;
}
Node * MyList::search(int key) {
	Node *tHead = head_;
    while (tHead->next_ != nullptr) {
        if (tHead->next_->data_ == key)
            return tHead->next_;
        tHead = tHead->next_;
    }
    return nullptr;
}
void MyList::erase(Node *pfind) {
    if (nullptr == pfind)
        return ;
    if (pfind->next_ != nullptr) {
        Node *temp = pfind->next_;
        pfind->data_ = temp->data_;
        pfind->next_ = temp->next_;
        delete temp;
        --len_;
    } else {
    	Node *tHead = head_;
        while (tHead->next_ != nullptr) {
            if (tHead->next_ == pfind) {
                tHead->next_ = pfind->next_;
                delete pfind;
                --len_;
                return ;
            }
            tHead = tHead->next_;
        }
    }
}
void MyList::sort() {
    Node *p, *q, *pre;
    for (int i = 0; i < len_ - 1; ++i) {
        pre = head_;
        p = head_->next_;
        q = p->next_;
        for (int j = 0; j < len_ - i - 1; ++j) {
            if (p->data_ > q->data_) {
                pre->next_ = q;
                p->next_ = q->next_;
                q->next_ = p;

                pre = q;
                q = p->next_;
                continue;
            }
            pre = pre->next_;
            p = p->next_;
            q = q->next_;
        }
    }
}
void MyList::invert() {
    Node *thead = head_->next_;
    head_->next_ = nullptr;
    while (thead) {
        Node *temp = thead->next_;
        thead->next_ = head_->next_;
        head_->next_ = thead;
        thead = temp;
    }
}
