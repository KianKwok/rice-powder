#include <iostream>
#include <vector>

using namespace std;

// resize > cursize push_back()
// resize < cursize pop_back()

class A {
public:
    // resize call parameterless constructor,
    // without parameterless constructor, function resize() error!
    A () {
        cout << "Parameterless constructor!       " << this << endl;
    }
    A (int data) {
        data_  = data;
        cout << "Constructor with parameter!      " << this << endl;
    }
    ~A () {
        cout << "Destructor!                      " << this << endl;
    }
    A(const A &another) {
        cout << "Copy constructor!                " << this << " = " << &another<< endl;
    }
    A & operator= (const A &another) {
        cout << "Assignment overload function!    " << this << " = " << &another << endl;
        return *this;
    }
    void setData(int data) {
        data_ = data;
    }
    int getData(void) {
        return data_;
    }
private:
    int data_;
};

void test1() {
    vector<A> va;
    va.reserve(100);
    va.push_back(A());
    va.resize(2);  // push_back()
    {
        va.resize(2);
    }
}
void test2_inefficient() {
    vector<A> va;
    A a;
    va.push_back(a);
    va.push_back(a);
    va.push_back(a);
}
void test2_better() {
    vector<A> va;
    va.reserve(10);
    A a;
    va.push_back(a);
    va.push_back(a);
    va.push_back(a);
}
void test3_insert() {
    // copy construct and copy assign
    vector<A> va;
    va.reserve(10);
    A a;
    va.push_back(a);
    va.push_back(a);
    va.push_back(a);

    va.insert(va.begin(), A());
}

void test4_insert_erase() {
    vector<A> va;
    va.reserve(10);
    A a;
    va.push_back(a);
    va.push_back(a);
    va.push_back(a);
# if 0
    // efficient
    va.push_back(a);
//    va.insert(va.end(), a); // equal
    {
        va.pop_back();
        va.erase(va.end());  // equal
        cout << "++++++++" << endl;
    }
# else
    // inefficient
    va.insert(va.begin(), a);
    {
        va.erase(va.begin());
        cout << "++++++" << endl;
    }
#endif
}

int main() {
//    test1();
//    test2_inefficient();
//    test2_better();
    test3_insert();
    cout << endl << "Hello World!" << endl;
    return 0;
}
