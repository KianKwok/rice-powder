#include <iostream>
#include <list>
#include <time.h>

using namespace std;

class A {
public:
    // resize call parameterless constructor,
    // without parameterless constructor, function resize() error!
    A () {
        cout << "Parameterless constructor!       " << this << endl;
    }
    A (int data) {
        data_  = data;
        cout << "Constructor with parameter!      " << this << endl;
    }
    ~A () {
        cout << "Destructor!                      " << this << endl;
    }
    A(const A &another) {
        this->data_ = another.data_;
        cout << "Copy constructor!                " << this << " = " << &another<< endl;
    }
    A & operator= (const A &another) {
        cout << "Assignment overload function!    " << this << " = " << &another << endl;
        if (this != &another)
            this->data_ = another.data_;
        return *this;
    }
    friend bool operator== (const A &one, const A &another) {
        return (one.data_ == another.data_);
    }
    friend bool operator< (const A &one, const A &another) {
        return (one.data_ < another.data_);
    }
    void setData(int data) {
        data_ = data;
    }
    int getData(void) {
        return data_;
    }
private:
    int data_;
};

void test_cmp() {
    list<A> li1 = {A(1), A(2), A(3), A(4)};
    list<A> li2 = {A(1), A(2), A(3), A(4)};

    // need friend bool operator== (const A &one, const A &another);
    if (li1 == li2)
        cout << " == " << endl;
    else
        cout << " != " << endl;
}
void test_emplace() {
    list<A> la;
    la.push_back(A());
    la.push_back(A(100));

    la.emplace(la.begin(), 200);

    for (auto &obj:la)
        cout << obj.getData() << " ";
    putchar(10);
}
void test_sort() {
    srand(time(nullptr));
    list<A> la;
    for (int i = 0; i < 10; ++i) {
        la.emplace_back(rand() % 100);
    }
    // need overload operator< ()
/**
  * not1  -----  noly one para ---- exam -- (int x) {return x > 5}
  * not2  -----  two para      ---- exam -- (int a, int b) {return a > b}
  */
#if 0
    la.sort();//     la.sort(less<A>());
#else
    la.sort(not2(less<A>()));
#endif
    for (auto &itr:la)
        cout << itr.getData() << " ";
    putchar(10);
}
void test_efficience_of_list() {
    list<A> li = {A(1), A(2)};
# if 0
    li.insert(li.begin(), A(100));
#else
    {
        li.erase(li.begin());
    }
#endif
    cout << "=================";
}

int main() {
//    test_cmp();
//    test_emplace();
//    test_sort();
    test_efficience_of_list();
    cout << "Hello World!" << endl;
    return 0;
}
