#include <iostream>
#include <vector>
#include <functional> // greater
#include <algorithm>  // count_if

using namespace std;
using namespace placeholders;  // _1

void test_bind1st_2nd() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};
    int count = -1;
//    count = count_if(vi.begin(), vi.end(), bind1st(greater<int>(), 3));// 2
//    count = count_if(vi.begin(), vi.end(), bind(greater<int>(), 3, _1)); // 2

//    count = count_if(vi.begin(), vi.end(), bind2nd(greater<int>(), 3)); // 7
    count = count_if(vi.begin(), vi.end(), bind(greater<int>(),_1, 3)); // 7
    cout << count << endl;
}
class A {
public:
    void print(int x, int y) {
        cout << "x = " << x << endl;
        cout << "y = " << y << endl;
    }
};
void test_bind_function() {
    A a;

    auto f1 = bind(&A::print, a, 1, 2);
    f1();

    auto f2 = bind(&A::print, _1, _2, _3);
    f2(a, 3, 5);

    auto f3 = bind(&A::print, a, _1, _2);
    f3(3, 5);
}

int main() {
	// test_bind1st_2nd();
	test_bind_function();
    cout << "Hello World!" << endl;
    return 0;
}
