#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

void dis(vector<int> &vi) {
    for (auto &i:vi)
        cout << i << " ";
    putchar(10);
}

void MakeHeapFun() {
    int arr[10] = {1,3,5,7,9,2,4,6,8,10};
    vector<int> vi(arr, arr + 10);

    make_heap(vi.begin(), vi.end());
    dis(vi);

    vi.push_back(100);
    dis(vi);

    make_heap(vi.begin(), vi.end());
    dis(vi);
}
void PopOneNode() {
    int arr[11] = {1,3,5,7,9,2,4,6,8,10,100};
    vector<int> vi(arr, arr + 11);
    make_heap(vi.begin(), vi.end());
    dis(vi);

    pop_heap(vi.begin(), vi.end());
    cout << "Pop node " << vi.back() << endl;
    vi.pop_back();
    dis(vi);
}
void PopAllNode() {
    int arr[11] = {1,3,5,7,9,2,4,6,8,10,100};
    vector<int> vi(arr, arr + 11);
    make_heap(vi.begin(), vi.end());
    dis(vi);

    while (vi.size() != 0) {
        pop_heap(vi.begin(), vi.end());
        cout << "Pop node " << vi.back() << endl;
        vi.pop_back();
    }
}
void SortHeap() {
    int arr[11] = {1,3,5,7,9,2,4,6,8,10,100};
    vector<int> vi(arr, arr + 11);
    make_heap(vi.begin(), vi.end());
//    make_heap(vi.begin(), vi.end(), less<int>()); // The default is less
//    make_heap(vi.begin(), vi.end(), greater<int>());
    dis(vi);

    sort_heap(vi.begin(), vi.end());
//    sort_heap(vi.begin(), vi.end(), less<int>()); // sort should in step with make
//    sort_heap(vi.begin(), vi.end(), greater<int>());
    dis(vi);

}

int main() {
    MakeHeapFun();
//    PopOneNode();
//    PopAllNode();
//    SortHeap();
    cout << "Hello World!" << endl;
    return 0;
}
