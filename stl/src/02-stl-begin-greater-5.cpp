#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;
class MyComlare {
public:
    bool operator() (int a) {
        return (a > 5);
    }
};
// MyComplare      // class template
// MyComplare<T>   // template class
// MyCompare<T> () // object

int main() {
    int arr[10] = {1,3,5,7,9,2,4,6,8,10};
    vector<int, allocator<int>> vi;
    vi.assign(arr, arr + 10);

    int count1 = count_if(vi.begin(), vi.end(),MyComlare());
    int count2 = count_if(vi.begin(), vi.end(), bind(greater<int> (), std::placeholders::_1, 5));
    cout << "count1 = " << count1 << endl;
    cout << "count2 = " << count2 << endl;


    cout << "Hello World!" << endl;
    return 0;
}
