#ifndef LIST_H
#define LIST_H


typedef struct _Node {
    int data_;
    struct _Node *next_;
} Node;

Node * createList(void);
void traversalList(Node *head);
int lenList(Node *head);

// insert to front
// insert to tail
// insert with sort
void insertList(Node *head, int data);
Node * searchList(Node *head, int key);

// traversal
// swap data
void eraseList(Node *head, Node *pfind);

// bubble sort swap data
// bubble sort swap pointer
void sortList(Node *head);
void invertList(Node *head);

// there are two equally complex ways.
void destoryList(Node *head);

#endif // LIST_H
