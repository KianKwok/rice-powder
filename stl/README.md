# STL

Standard Template Library

Introduce

* [vector traversal](./src/01-stl-begin.cpp)
* [count if](./src/02-stl-begin-greater-5.cpp)
* [container](./src/03-container.cpp)

Vector

* [basic function](./src/04-vector.cpp) : insert(), push(), pop(), assign(). 
* [no modify function](./src/05-vector-nomodify.cpp) : size(), capacity(), max_size().

* [iterator](./src/06-vector-iterator.cpp) : traversal vector with iterator, begin(), rbegin(), cbegin().
* [insert erase resize](./src/07-vector-insert-erase-resize.cpp)
* [exercise](./src/08-vector-exercise.cpp)
* [capacity](./src/09-vector-capacity.cpp)
* [object class](./src/10-vector-obj.cpp)
* [pointer of object class](./src/11-vector-obj-pointer.cpp)
* [invalid iterator](./src/12-vector-invalid-iterator.cpp)
* [Multidimensional vector](./src/13-vector-two-dim.cpp)

List

* [list basic](./src/14-list-begin.cpp) : push(), pop(), remove(), construct, empty(), insert(), erase().
* [list special operation](./src/15-list-special-operation.cpp) : sort(), unique(), splice(), reverse(), merge().
* [list exercise](./src/16-list-exercise.cpp)
* [list object class](./src/17-list-object.cpp) : compare, emplace(), efficiency.

Container adapter

* [deque](./src/18-deque.cpp)
* [stack](./src/19-stack.cpp)
* [queue](./src/20-queue.cpp)
* [priority queue](./src/21-priority-queue.cpp)

Associative  container

* [map](./src/22-map.cpp)
* [set](./src/23-set.cpp)

Iterator

* [auxiliary iterator](./src/24-auxiliary-iterator.cpp)
* [ostream iterator](./src/25-ostream-iterator.cpp)

Functional

* [bind](./src/26-bind.cpp)
* [bind member function](./src/27-bind-memeber-function.cpp)

Algorithm

* [sort](./src/28-sort.cpp)
* [partition](./src/29-partition.cpp)
* [random shuffle](./src/30-random-shuffle.cpp)
* [find](./src/31-find.cpp)
* [search](./src/32-search.cpp)
* [remove](./src/33-remove.cpp)
* [unique](./src/34-unique.cpp)
* [set algorithm](./src/35-set-algorithm.cpp)

Other

* [run time](./src/36-run-time.cpp)