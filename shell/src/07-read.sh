#########################################################################
# File Name: 07-read.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Thu Feb 13 00:44:36 2020
#########################################################################
#!/bin/bash

read name
echo "the input is : ${name}"

read first second
echo "first is : ${first}, second is : ${second}"

read
echo "REPLY is : ${REPLY}"

read -a array
# input : 1 2 3 4 5

echo "==============="
read -p "please input temp0 : " temp0
read -p "you can input at most three char : " -n 3 temp1
read -p "you need input in 5 seconds" -t 5 temp2
read -p "input password : " -s passwd
read -p "input with end ':'" -d ':' temp3 
