#include "Food.h"
#include <stdlib.h>

Food::Food(Snake &s)
    :snake_(s) {}

void Food::setFood() {
    pos_.x_ = rand() % 14 + 1;
    pos_.y_ = rand() % 14 + 1;
}
Pos Food::getPos() {
    return pos_;
}
bool Food::isFood(const Pos &p) {
    return (pos_ == p);
}
