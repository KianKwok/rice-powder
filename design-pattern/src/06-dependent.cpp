#include <iostream>

using namespace std;

class Tool {
public:
    virtual void useTool() = 0;
};
class Boat : public Tool {
public:
    void useTool() {
        cout << "use Boat" << endl;
    }
};
class Plane : public Tool {
    void useTool() {
        cout << "use plane" << endl;
    }
};
class Person {
public:
    void traverse(Tool *t) {
        t->useTool();
    }
};

int main() {
    Person pe;
    Boat bo;
    Plane pl;

    pe.traverse(&bo);
    pe.traverse(&pl);

    cout << "Hello World!" << endl;
    return 0;
}
