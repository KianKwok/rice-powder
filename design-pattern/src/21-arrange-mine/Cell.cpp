#include "Cell.h"

Cell::Cell() {
    mine_ = 0;
    status_ = BLANK;
}
void Cell::setMine(int mine) {
    mine_ = mine;
}
int Cell::getMine() {
    return mine_;
}
void Cell::setStatus(CellStatus status) {
    status_ = status;
}
CellStatus Cell::getStatus() {
    return status_;
}
