#ifndef SCREEN_H
#define SCREEN_H


class Snake;
class Food;

class Screen {
public:
    Screen();
    void screenFlush(Snake *s, Food *f);
};

#endif // SCREEN_H
