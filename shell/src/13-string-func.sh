#########################################################################
# File Name: 13-string-func.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 23:14:57 2020
#########################################################################
#!/bin/bash

str="abcd"
echo "This length of str is : ${#str}"

str2="abcdefg hijklmn"
echo ${str2:2:4}
# cdef
echo ${str2:5}
# fg hijklmn

a="abc"
b="abc"

if [ ${a} = ${b} ]
then
	echo "a == b"
else
	echo "a != b"
fi
# if [ ${a} != ${b} ]

if [ -z ${a} ]
then
	echo "a is null"
else
	echo "a is not null"
fi

if [ ${a} ]
then
	echo "a is exist"
fi

if [ ${c} -a ${c} = "abc" ]
then
	echo "c = \"abc\""
else
	echo "c is not exist or c != \"abc\""
fi
