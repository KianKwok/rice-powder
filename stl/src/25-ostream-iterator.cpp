#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric> // std::iota

using namespace std;

void test_copy() {
    vector<int> vi(10);

    iota(vi.begin(), vi.end(), 1);

    for (auto &i:vi)
        cout << i << " ";
    putchar(10);

    vector<int> vi2(15);
    copy(vi.begin(), vi.end(), vi2.begin());
    for (auto &i:vi2)
        cout << i << " ";
    putchar(10);
}
void test_ostream() {
    ostream_iterator<int> intWriter(cout, " ");

    *intWriter = 42;
    ++intWriter;
    *intWriter = 77;
    ++intWriter;
    *intWriter = -5;

    vector<int> coll = {1,2,3,4,5,6,7,8,9};
    copy(coll.begin(), coll.end(), ostream_iterator<int>(cout));
    putchar(10);

    copy(coll.cbegin(), coll.cend(), ostream_iterator<int>(cout, " "));
    putchar(10);

}

int main() {
    test_copy();
    test_ostream();
    cout << "Hello World!" << endl;
    return 0;
}
