#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool isodd(int i) {
    return (i % 2);
}

int main() {
    int data[] = {1,2,3,4,5,6,7,8,9};

    vector<int> vi(data, data + 10);
    auto itr = partition(vi.begin(), vi.end(), isodd);
//    stable_partition(vi.begin(), vi.end(), isodd);

    cout << *itr << endl;

    sort(vi.begin(), itr, less<int>());
    for (auto &i:vi)
        cout << i << " ";
    putchar(10);
    cout << "Hello World!" << endl;
    return 0;
}
