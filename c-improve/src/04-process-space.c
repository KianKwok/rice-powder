#include <stdio.h>
/*
*************************
*************************
high address | system
             | args env // command-line arguments and environment variables
             | stack
             | ...
             | heap
             | uninitialized data (bss)
             | initialized data
low address  | text
*************************
*************************
*/
int a;                     // data -> uninit -> (bss)
static int b;              // data -> uninit -> (bss)
int c = 4;                 // data -> init -> rw
static int d = 6;          // data -> init -> rw
char *PP = "china";        //       p : data -> init -> rw
                           // "china" : data -> init -> ro
void func() {
    static int si;        // data -> uninit -> (bss)
    int var;              // stack
}
int main(int argc, char *argv[], char *env[]) {
    while (*env) {
        printf("%s\n", *env++);
    }

    static int s = 6;      // data -> init -> rw
    int var = 5;           //           stack
    int arr[3] = {0,1,2};  //     arr : stack
    char * p = "china";    //       p : stack
                           // "china" : data -> init -> ro

    func();
    printf("Hello world!\n");
    return 0;
}
// Constants may be in Area "text" or in Area "data -> init -> ro"
