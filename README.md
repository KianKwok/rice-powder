# Mr. Wang

The process of learning programming from Mr. Wang on [Nzhsoft](http://edu.nzhsoft.cn/)</br>

## Catalog

### Basic

* [C Foundation](./c-foundation/)
* [C Improvement](./c-improve/)
* [C++](./cpp/)
* [STL](./stl/)
* Linux Foundation
    * [Shell Program](./shell/)
* Linux System Programming

### Junior

* [DSAA](./data-struct/)
: Data structure and algorithms.
* [Design Pattern](./design-pattern/)
* [Xml & Json](./xml-json/)
* Qt
* Linux + Qt Competitive Training
* Mysql
* Tinyhttpd
* LibEvent

### Advanced

* Lua
