#include <iostream>

using namespace std;
class GrandFather {
public:
    virtual void test() {
        cout << "GrandFather test()" << endl;
    }
};
class Father : public GrandFather {
public:
    virtual void test() {
        cout << "Father test()" << endl;
    }
};
class Son : public Father {
public:
    void test() {
        GrandFather::test();
        Father::test();
        cout << "Son test()" << endl;
    }
};

int main() {
    GrandFather *pg = new Son;
    pg->test();
    cout << "Hello World!" << endl;
    return 0;
}
