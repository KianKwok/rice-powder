#########################################################################
# File Name: 09-operator-arithm.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Thu Feb 13 01:00:26 2020
#########################################################################
#!/bin/bash

a=10
b=5
sum=$(($a + $b))
sub=$(($a - $b))
mul=$(($a * $b))
div=$(($a / $b))
mod=$(($a % $b))
echo "sum : ${sum}"

c=$a
echo "c : ${c}"
