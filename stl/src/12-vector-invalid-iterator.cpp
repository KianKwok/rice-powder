#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void test1_erase_invalid() {
    vector<int> vi;
	vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        if (!(*itr % 2))
            vi.erase(itr);
    }
    for (auto &i: vi)
        cout << i << " ";
    putchar(10);
}
void test1_erase_valid() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ) {
        if (((*itr) % 2) == 0)
            itr = vi.erase(itr);
        else
            ++itr;
    }
    for (auto &i: vi)
        cout << i << " ";
    putchar(10);
}
void better_erase() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    for (auto &i:vi)
        cout << i << " ";
    putchar(10);

    vi.erase(remove(vi.begin(), vi.end(), 5), vi.end());

    for (auto &i: vi)
        cout << i << " ";
    putchar(10);
}
void test_romeve() {
	vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

	// 1 2 3 4 5 6 7 8 9 10
	cout << "size : " << vi.size() << endl;   // 10
	remove(vi.begin(), vi.end(), 5);
	// 1 2 3 4 6 7 8 9 10 10
	cout << "size : " << vi.size() << endl;   // 10
}
void test2_insert_invalid() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ++itr) {
        if (!(*itr % 2))
            vi.insert(itr, 100);
    }
    for (auto &i: vi)
        cout << i << " ";
    putchar(10);

}
void test2_insert_valid() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9,10};

    vector<int>::iterator itr;
    for (itr = vi.begin(); itr != vi.end(); ) {
        if (((*itr) % 2) == 0) {
            itr = vi.insert(itr, 100);
            itr += 2;
        } else {
            ++itr;
        }
    }
    for (auto &i: vi)
        cout << i << " ";
    putchar(10);
}


int main() {
//    test1_erase_invalid();
//    test1_erase_valid();
//    test2_insert_invalid();
//    test2_insert_valid();
    better_erase();
    test_romeve();
    cout << "Hello World!" << endl;
    return 0;
}
