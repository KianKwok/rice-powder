#include <stdio.h>
#include <string.h>

void SmallDataToBig(void);
void BigDataToSamll(void);
void SignAndUnsign(void);
void CharAndInt(void);

int main() {
//    SmallDataToBig();
//    BigDataToSamll();
//    SignAndUnsign();
    CharAndInt();
    printf("Hello World!\n");
    return 0;
}

void SmallDataToBig(void) {
    char a = (signed char)0xff; //                               1111 1111
    int b = a;                  // 1111 1111 1111 1111 1111 1111 1111 1111
    printf("b = %d\n", b);

    char aa = 0x7f;             //                               0111 1111
    int bb = aa;                // 0000 0000 0000 0000 0000 0000 0111 1111
    printf("bb = %d\n", bb);
}
void BigDataToSmall(void) {     // truncate
    int a = 0xff;               // 0000 0000 0000 0000 0000 0000 1111 1111
    char b = (char)a;           //                               1111 1111
    printf("b = %d\n", b);

    int aa = 256;               // 0000 0000 0000 0000 0000 0001 0000 0000
    char bb = (char)aa;         //                               0000 0000
    printf("bb = %d", bb);
}
// long double > double > float > unsigned int > int > other
void SignAndUnsign(void) {
    unsigned int a = 1;
    int b = -100;
    printf("a + b = %d\n", a + b);
    printf("a + b = %u\n", a + b);
    if (a + b > a) {
        printf("a + b > a --- unsigned\n");
    } else {
        printf("a + b < a --- signed\n");
    }
}
void CharAndInt(void) {
    char a[10000] = {0};
    for (int i = 0; i < 10000; ++i) {
        a[i] = -1 - i;
    }
//    strcpy(a, "abc");
    printf("%d\n", strlen(a));
}
