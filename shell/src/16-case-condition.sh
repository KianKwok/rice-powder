#########################################################################
# File Name: 16-case-condition.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 23:43:43 2020
#########################################################################
#!/bin/bash

option=$1
case ${option} in
	"‐f")
		echo "param is ‐f"
		;;
	‐d) 
		echo "param is ‐d"
		;;
	*)
		echo "$0:usage: [‐f ] | [ ‐d ]"
		exit 1
		;;
esac
