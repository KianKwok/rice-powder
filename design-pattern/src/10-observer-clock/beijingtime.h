#ifndef BEIJINGTIME_H
#define BEIJINGTIME_H


#include "subject.h"

class BeijingTime : public Subject {
public:
    BeijingTime();
    void setTime(int hour, int min, int sec);
    void dis();
    void notify();
protected:
    int hour_;
    int min_;
    int sec_;
};

#endif // BEIJINGTIME_H
