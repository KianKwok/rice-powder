#include "config.h"
#include <fstream>

Config *Config::ins_ = nullptr;

Config * Config::getInstance() {
    if (nullptr == ins_)
        ins_ = new Config;
    return ins_;
}
Config::Config() {
    fstream fs;
    fs.open("com.cof", ios::in|ios::out);
    if (!fs)
        cout << "Open error!" << endl;
    fs >> infor.ip_;
    fs >> infor.port_;
    fs.close();
}
string Config::getIp() {
    return infor.ip_;
}
string Config::getPort() {
    return infor.port_;
}

