# DSAA

Data Structure and Algorithms

***

## Data Structure

List

* [list](./src/01-list/)
: The one-way linked list.
* [double list](./src/04-double-list/)
: The doubly-linked list.
* [judge ring](./src/22-list-has-ring.c)
: Judge whether a linked list has ring.
* [get mid node](./src/23-list-mid-node.c)
: To find the middle node of a linked list, only one loop is required.
* [Josephus problem](./src/24-josephus.c)
: The Josephus Problem, with one-way circular list.
* [list class](./src/02-list-class/)
: The C++ class of one-way linked list.
* [list \<T> class](./src/03-list-T-class/)
: The C++ template class of one-way linked list.

Stack

* [stack array](./src/19-stack.c)
: Implementing stack with array.
* [stack list](./src/20-stack-list.c)
: Implementing stack with linked list.
* [one array double stack](./src/20-stack-list.c)
* [map of maze](./src/21-deep-search.c)
: Deep first search to print the map of a maze.
* [way of maze](./src/25-deep-search.c)
: Deep first search to get the way of a maze.
* [stack class](./src/20-stack-list.c)
* [stack \<T> class](./src/20-stack-list.c)

Queue

Tree

* [bst-traversal](./src/01traverse_recursion_BST.c)
* [bst-traversal](./src/02traverse_iteration_BST.c)
* [bst-create](./src/03create_BST.c)
* [bst-search](./src/04search_node_BST.c)
* [bst max and min](./src/05max_min_node_BST.c)
* [bst get parent](./src/06get_parent_recursion_in_BST.c)
* [bst get parent](./src/01traverse_recursion_BST.c)
* [bst erase](./src/01traverse_recursion_BST.c)
* [bst destory](./src/01traverse_recursion_BST.c)

Huffman

Heap in STL

* [stl heap](./src/11-heap-stl.cpp)

Shell

***

## Sort Algorithms

Bubble Sort

* [bubble sort](./src/12-bubble-sort.c)
* [bubble sort improve](./src/13-bubble-sort-improve.c)

Insert Sort

* [insert sort](./src/14-insert-sort.c)

Shell Sort

* [shell sort](./src/15-shell-sort.c)

Select Sort

Quick Sort

Merge Sort

***

## Search Algorithms
