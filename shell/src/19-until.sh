#########################################################################
# File Name: 19-until.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Sat Feb 15 00:06:28 2020
#########################################################################
#!/bin/bash

count=0
until [ ${count} -eq 5 ]
do
	count=$((${count} + 1))
	echo ${count}
done
