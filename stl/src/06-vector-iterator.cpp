#include <iostream>
#include <vector>

using namespace std;

void test1() {
    vector<int> vi = {1,2,3,4,5,6};

    vector<int>::iterator itr1;  // do not recommend
    for (itr1 = vi.begin(); itr1 != vi.end(); ++itr1)
        cout << *itr1 << " ";
    putchar (10);

    for (auto itr = vi.begin(); itr != vi.end(); ++itr)
        cout << *itr << ' ';
    putchar(10);

    for (auto itr = vi.rbegin(); itr != vi.rend(); ++itr)
        cout << *itr << ' ';
    putchar(10);

    for (auto itr = vi.begin(); itr != vi.end(); ++itr)
        *itr += 100;
    putchar(10);
    for (auto itr = vi.cbegin(); itr != vi.cend(); ++itr)
//        *itr += 100; // c is const
    putchar(10);
    for (auto itr = vi.crbegin(); itr != vi.crend(); ++itr)
//        *itr += 100; // c is const
    putchar(10);
}

void test2 () {
    vector<int> vi = {1,2,3,4,5,6};
#if 0
    // vector<int>::iterator itr;
    vector<int>::reverse_iterator itr;
    // vector<int>::const_iterator itr;
    // vector<int>::const_reverse_iterator itr;
#else
    auto itr;
#endif
    for (itr = vi.rbegin(); itr != vi.rend(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
}

int main() {
    test1();
    test2();
    cout << "Hello World!" << endl;
    return 0;
}

