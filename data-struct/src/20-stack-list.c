#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
    char data;
    struct _Node *next;
} Node;
typedef struct _Stack {
    Node *top;
} Stack;

void initStack(Stack *s);
int isEmptyStack(Stack *s);
void pushStack(Stack *s, char data);
char popStack(Stack *s);
void resetStack(Stack *s);
void destoryStack(Stack *s);

int main() {
    Stack s;
    printf("Hello World!\n");
    initStack(&s);

    for (char ch = 'A'; ch <= 'Z'; ++ch) {
        pushStack(&s, ch);
    }
    while (!isEmptyStack(&s)) {
        printf("%c ", popStack(&s));
    }
    putchar(10);
    destoryStack(&s);
    printf("Hello World!\n");
    return 0;
}

void initStack(Stack *s) {
    s->top = (Node *)malloc(sizeof(Node));
    s->top->next = NULL;
}
int isEmptyStack(Stack *s) {
    return (s->top->next == NULL);
}
void pushStack(Stack *s, char data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    temp->data = data;
    temp->next = s->top->next;
    s->top->next = temp;
}
char popStack(Stack *s) {
    Node *temp = s->top->next;
    char ch = temp->data;
    s->top->next = temp->next;
    free(temp);
    return ch;
}
void resetStack(Stack *s) {
    while (!isEmptyStack(s)) {
        popStack(s);
    }
}
void destoryStack(Stack *s) {
    resetStack(s);
    free(s->top);
}
