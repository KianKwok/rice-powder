#include <iostream>

using namespace std;

class Singleton {
public:
    static Singleton * getInstance() {
        if (ins_ == nullptr)
            ins_ = new Singleton;
        return ins_;
    }
private:
    static Singleton *ins_;
    Singleton() {}
    Singleton(const Singleton &) {}
//    Singleton & operator=(const Singleton &) {} // Don't need
    ~Singleton() {}
};

Singleton * Singleton::ins_ = nullptr;

int main() {
    Singleton *ps1 = Singleton::getInstance();
    Singleton *ps2 = Singleton::getInstance();

    if (ps1 == ps2)
        cout << "ps1 == ps2" << endl;

    cout << "Hello World!" << endl;
    return 0;
}
