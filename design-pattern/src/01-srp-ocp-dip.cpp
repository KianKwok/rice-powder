#include <iostream>

using namespace std;

#if 0
class Book {
public:
    string getContents(void) {
        return "Story of book";
    }
};
class Paper {
public:
    string getContents(void) {
        return "Story of paper";
    }

};
class Mother {
public:
    void tellStory(Book *b) {
        cout << b->getContents() << endl;
    }
    void tellStory(Paper *p) {
        cout << p->getContents() << endl;
    }
};
#else
class IReader {
public:
    virtual string getContents(void);
};
class Book:public IReader {
public:
    string getContents(void) {
        return "Story of book";
    }
};
class Paper:public IReader {
public:
    string getContents(void) {
        return "Story of paper";
    }

};
class Mother {
public:
    void tellStory(IReader *ir) {
        cout << ir->getContents() << endl;
    }
};
#endif

int main() {
    Book b;
    Paper p;
    Mother m;
    m.tellStory(&b);
    m.tellStory(&p);
    cout << "Hello World!" << endl;
    return 0;
}
