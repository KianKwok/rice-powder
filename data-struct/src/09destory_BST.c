#include <stdio.h>
#include <stdlib.h>

typedef struct _TreeNode {
    struct _TreeNode *right_;
    struct _TreeNode *left_;
    int data_;
} TreeNode;

void MidOrder(TreeNode *r);
void insertBst_recursion(TreeNode * * r, int data);

void DestoryBST(TreeNode *r) {
    if (r != NULL) {
        DestoryBST(r->left_);
        DestoryBST(r->right_);
        free(r);
        r = NULL;
    }
}

int main() {
    TreeNode *root = NULL;
    insertBst_recursion(&root, 30);
    insertBst_recursion(&root, 8);
    insertBst_recursion(&root, 15);
    insertBst_recursion(&root, 36);
    insertBst_recursion(&root, 100);
    insertBst_recursion(&root, 32);
    MidOrder(root);
    putchar(10);

    DestoryBST(root);
    root = NULL;
    MidOrder(root);
    putchar(10);

    printf("Hello World!\n");
    return 0;
}
void MidOrder(TreeNode *r) {
    if (r) {
        MidOrder(r->left_);
        printf("%d ", r->data_);
        MidOrder(r->right_);
    }
}
void insertBst_recursion(TreeNode * * r, int data) {
    if (*r == NULL) {
        *r = (TreeNode *)malloc(sizeof(TreeNode));
        (*r)->data_ = data;
        (*r)->left_ = NULL;
        (*r)->right_ = NULL;
    } else if (data > (*r)->data_) {
        insertBst_recursion(&(*r)->right_, data);
    } else {
        insertBst_recursion(&(*r)->left_, data);
    }
}
