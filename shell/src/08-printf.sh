#########################################################################
# File Name: 08-printf.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 22:43:23 2020
#########################################################################
#!/bin/bash

printf "Hello World\n"
#echo "Hello World"

printf "%d %s\n" 123 "abc"
#123 abc

printf '%d %s\n' 123 "abc"
#123 abc

printf "%d %s\n" 123 "abc" 456 "def"
#123 abc
#456 def

printf "%d %s\n"
#0
