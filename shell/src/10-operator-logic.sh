#########################################################################
# File Name: 10-operator-logic.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 12:29:15 2020
#########################################################################
#!/bin/bash

a=10
b=5
if [ $a -eq $b ]
then
	echo "a == b"
else
	echo "a != b"
fi
# if [ $a -eq $b ]
# if [ $a == 10 ]
# if [ $a -ne $b ] not equal
# if [ $a != 10 ]
# if [ $a -gt $b ] greater than
# if [ $a -lt $b ]
# if [ $a -ge $b ]
# if [ $a -le $b ]

# 0<=a && a<=20
#if [ $a -ge 0 -a $a -le 20 ]

# a<0 || 20<a
#if [ $a -lt 0 -o $a -gt 20 ]

#if [ ! $a -gt 20 ]
