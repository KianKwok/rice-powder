#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

template<typename T>
bool inQuote(const T &cont, const string &s) {
    return (search(cont.begin(), cont.end(), s.begin(), s.end()) != cont.end());
}

void test1() {
    string str = "why waste time learning, when ignorance is instantaneous?";
    cout << boolalpha << inQuote(str, "learning") << endl;
    cout << boolalpha << inQuote(str, "leeening") << endl;
}
void test2() {
    string str = "why waste time learning, when ignorance is instantaneous?";
    vector<char> vec(str.begin(), str.end());
    cout << boolalpha << inQuote(vec, "learning") << endl;
    cout << boolalpha << inQuote(vec, "leeening") << endl;
}

template <class Cont, class Size, class T>
bool consecutive_values(const Cont &c, Size count, const T &v) {
    return (search_n(begin(c), end(c), count, v) != end(c));
}
void test3() {
    const char sequence[] = "10100100010001000100101";
    cout << boolalpha;
    cout << "have 4 consecutive zero " << consecutive_values(sequence, 4, '0') << endl;
    cout << "have 3 consecutive zero " << consecutive_values(sequence, 3, '0') << endl;
}

int main() {
//    test1();
//    test2();
    test3();
    cout << "Hello World!" << endl;
    return 0;
}
