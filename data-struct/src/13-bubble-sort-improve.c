#include <stdio.h>

void BubbleSort(int *arr, int len) {
    int flag = -1;
    for (int i = 0; i < len - 1; ++i) {
        flag = 1;
        for (int j = 0; j < len - i - 1; ++j) {
            if (arr[j] > arr[j + 1]) {
                arr[j] ^= arr[j + 1];
                arr[j + 1] ^= arr[j];
                arr[j] ^= arr[j + 1];
                flag = 0;
            }
        }
        if (flag)
            break;
    }
}
void display(int *arr, int len) {
    for (int i = 0; i < len; ++i) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int arr[] = {1,3,5,7,9,2,4,6,8};
    BubbleSort(arr, sizeof(arr)/sizeof(*arr));
    display(arr, sizeof(arr)/sizeof(*arr));
    putchar(10);

    printf("Hello World!\n");
    return 0;
}
