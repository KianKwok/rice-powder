#include <iostream>
#include "tinyxml2.h"

using namespace std;
using namespace tinyxml2;

/**
<?xml version="1.0" encoding="utf-8"?>
<!-- This is a XML comment -->
<bookstore>
    <book category="COOKING">
        <title lang="en">Everyday Italian</title>
        <author>Giada De Laurentiis</author>
        <year>2005</year>
        <price>30.00</price>
    </book>
</bookstore>
 */

int main() {
    XMLDocument doc;
    XMLDeclaration *pdec1 =
            doc.NewDeclaration("xml version=\"1.0\" encoding=\"utf-8\"");

    doc.LinkEndChild(pdec1);

    doc.LinkEndChild(doc.NewComment("This is a XML comment"));

    XMLElement *pEbookstore = doc.NewElement("bookstore");
        XMLElement *pEbook = doc.NewElement("book");
        pEbook->SetAttribute("category", "COOKING");

            XMLElement *pEtitle = doc.NewElement("title");
            pEtitle->SetAttribute("lang", "en");
            pEtitle->SetAttribute("id", "123456");
            pEtitle->SetText("Everyday Italian");
            pEbook->LinkEndChild(pEtitle);

            XMLElement *peauthor = doc.NewElement("author");
            peauthor->SetText("Giada De Laurentiis");
            pEbook->LinkEndChild(peauthor);

            XMLElement *peyear = doc.NewElement("year");
            peyear->SetText("2005");
            pEbook->LinkEndChild(peyear);

            XMLElement *peprice = doc.NewElement("price");
            peprice->SetText("30.00");
            pEbook->LinkEndChild(peprice);

        pEbookstore->LinkEndChild(pEbook);
    doc.LinkEndChild(pEbookstore);
    doc.Print();
    doc.SaveFile("xxx.xml");
    cout << "Hello World!" << endl;
    return 0;
}
