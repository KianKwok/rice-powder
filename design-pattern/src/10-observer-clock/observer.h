#ifndef OBSERVER_H
#define OBSERVER_H


#include <iostream>

using namespace std;

class Observer {
public:
    Observer();
    virtual void uplate(int hour, int min, int sec) = 0;
    void dis();
protected:
    int hour_;
    int min_;
    int sec_;
};

#endif // OBSERVER_H
