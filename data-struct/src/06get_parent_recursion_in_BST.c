#include <stdio.h>
#include <stdlib.h>

typedef struct _TreeNode {
    struct _TreeNode *right_;
    struct _TreeNode *left_;
    int data_;
} TreeNode;

void MidOrder(TreeNode *r);
void insertBst_recursion(TreeNode * * r, int data);
TreeNode * SearchBSTLoop(TreeNode *r, int key);

TreeNode * GetParentNode(TreeNode *r, TreeNode *child) {
    static TreeNode *parent = NULL;
    if (r) {
        if (r->left_ == child || r->right_ == child) {
            parent = r;
        }
        GetParentNode(r->left_, child);
        GetParentNode(r->right_,child);
    }
    return parent;
}

int main() {
    TreeNode *root = NULL;
    insertBst_recursion(&root, 30);
    insertBst_recursion(&root, 8);
    insertBst_recursion(&root, 15);
    insertBst_recursion(&root, 36);
    insertBst_recursion(&root, 100);
    insertBst_recursion(&root, 32);
    MidOrder(root);
    putchar(10);

    TreeNode *pFind = SearchBSTLoop(root, 100);
    if (pFind != NULL) {
        printf("Yes, the numb is %d\n", pFind->data_);
        printf("The parent is %d\n", GetParentNode(root, pFind)->data_);
    }
    else
        printf("No, can't find the numb\n");


    printf("Hello World!\n");
    return 0;
}

void MidOrder(TreeNode *r) {
    if (r) {
        MidOrder(r->left_);
        printf("%d ", r->data_);
        MidOrder(r->right_);
    }
}
void insertBst_recursion(TreeNode * * r, int data) {
    if (*r == NULL) {
        *r = (TreeNode *)malloc(sizeof(TreeNode));
        (*r)->data_ = data;
        (*r)->left_ = NULL;
        (*r)->right_ = NULL;
    } else if (data > (*r)->data_) {
        insertBst_recursion(&(*r)->right_, data);
    } else {
        insertBst_recursion(&(*r)->left_, data);
    }
}
TreeNode * SearchBSTLoop(TreeNode *r, int key) {
    while (r) {
        if (r->data_ == key)
            return r;
        else if (r->data_ > key)
            r = r->left_;
        else
            r = r->right_;
    }
    return NULL;
}
