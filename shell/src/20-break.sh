#########################################################################
# File Name: 20-break.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Sat Feb 15 00:09:08 2020
#########################################################################
#!/bin/bash

for i in 1 2 3 4 5
do
	if [ $i -eq 3 ]
	then
		break
	else
		echo $i
	fi
done

for i in 1 2 3 4 5
do
	for j in 1 2 3 4 5
	do
		if [ $i -eq 2 -a $j -eq 3 ]
		then
			break 2
		else
			echo "($i, $j)"
		fi
	done
done
