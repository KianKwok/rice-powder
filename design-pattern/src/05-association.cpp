#include <iostream>
#include <vector>

using namespace std;

class Student;

class Teacher {
public:
    void addStudent(Student *s) {
        vs_.push_back(s);
    }
private:
    vector<Student *> vs_;
};
class Student {
public:
    void addTeacher(Teacher *t) {
        vt_.push_back(t);
    }
private:
    vector<Teacher *> vt_;
};

int main() {
    Student *s1 = new Student;
    Student *s2 = new Student;
    Student *s3 = new Student;
    Student *s4 = new Student;
    Teacher *t1 = new Teacher;
    Teacher *t2 = new Teacher;
    Teacher *t3 = new Teacher;
    Teacher *t4 = new Teacher;

    s1->addTeacher(t1);
    s1->addTeacher(t2);
    s1->addTeacher(t3);
    s1->addTeacher(t4);

    t1->addStudent(s1);
    t1->addStudent(s2);
    t1->addStudent(s3);
    t1->addStudent(s4);

    cout << "Hello World!" << endl;
    return 0;
}
