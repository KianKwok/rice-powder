# Design Pattern

## Principle

* [SRP OCP DIP](./src/01-srp-ocp-dip.cpp)
    * SRP : Single Responsibility Principle
    * OCP : Open Closed Principle
    * DIP : Dependence Inversion Principle 
* [ISP](./src/02-isp.cpp) : Interface Segregation Principle

## UML

* [Function and Object](./src/03-function-object.cpp)
* [Composition & Aggrefation](./src/04-composition-aggregation.cpp)
* [Association](./src/05-association.cpp)
* [Dependent](./src/06-dependent.cpp)

## Singleton

* [Singleton Mode](./src/07-singleton.cpp)
* [Singleton Config](./src/08-singleton-config/)

## Observer

* [Without Observer](./src/09-observer-without.cpp)
* [Observer Clock](./src/10-observer-clock/)

## Strategy

* [Strategy](./src/11-strategy.cpp)

## Proxy

* [Proxy  Smart Pointer](./src/12-proxy-smart.cpp)
* [Proxy Document and Image](./src/13-proxy-doc.cpp)

## Decorator

* [Polymorphism](./src/14-polymorphism.cpp)
* [Decorate](./src/15-decorate.cpp)

## Adapter

* [Wolf Sheep](./src/16-adapter-wolf-sheep.cpp)
* [Adapter STL](./src/17-adapter-stl.cpp) : The stack and queue in STL is adapter

## Bridge

* [Shape and Line](./src/18-bridge.cpp)

## Factory

* [Simple Factory](./src/19-factory-simple.cpp)
* [Improve Factory](./src/20-factory-improve.cpp)

## MVC

Model View Controller

* [Arrange Mine](./src/21-arrange-mine/) : Arrange mines in the Game Minesweeper
* [Snake](./src/22-snake/) : The foundation model of Game Snake