#include "doublelist.h"
#include <stdio.h>
#include <stdlib.h>

Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->pre_ = head;
    head->next_ = head;
    return head;
}
void insertList(Node *head, int data) {
    if (NULL == head)
        return ;
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;
    temp->pre_ = head;
    temp->next_ = head->next_;

    head->next_ = temp;
    temp->next_->pre_ = temp;
}
void traversalList(Node *head) {
    if (NULL == head)
        return ;
    Node *temp = head->next_;
    while (temp != head) {
        printf("%d ", temp->data_);
        temp = temp->next_;
    }
    putchar(10);
}
Node * searchList(Node *head, int key) {
    if (NULL == head)
        return NULL;
    Node *temp = head->next_;
    while (temp != head) {
        if (temp->data_ == key)
            return temp;
        temp = temp->next_;
    }
    return NULL;
}
void eraseList(Node *head, Node *pfind) {
    if (NULL == head || NULL == pfind)
        return ;
    Node *temp = head;
    while (temp->next_ != head) {
        if (temp->next_ == pfind) {
            temp->next_ = pfind->next_;
            pfind->next_->pre_ = temp;
            free(pfind);
            return ;
        }
        temp = temp->next_;
    }
}
int lenList(Node *head) {
    if (NULL == head)
        return -1;
    int len = 0;
    Node *temp = head->next_;
    while (temp != head) {
        ++len;
        temp = temp->next_;
    }
    return len;
}

#if 0
// swap data
void sortList(Node *head) {
    if (NULL == head)
        return ;
    int len = lenList(head);
    for (int i = 0; i < len - 1; ++i) {
        int flag = 1;
        Node *temp = head->next_;
        for (int j = 0; j < len - i - 1; ++j) {
            if (temp->data_ > temp->next_->data_) {
                temp->data_ ^= temp->next_->data_;
                temp->next_->data_ ^= temp->data_;
                temp->data_ ^= temp->next_->data_;
                flag = 0;
            }
            temp = temp->next_;
        }
        if (flag == 1)
            break;
    }
}
#else
// swap pointer
void sortList(Node *head) {
    if (NULL == head)
        return ;
    int len = lenList(head);
    for (int i = 0; i < len - 1; ++i) {
        int flag = 1;
        Node *temp = head->next_;
        for (int j = 0; j < len - i - 1; ++j) {
            if (temp->data_ > temp->next_->data_) {
                temp->pre_->next_ = temp->next_;
                temp->next_ = temp->next_->next_;
                temp->pre_->next_->next_ = temp;

                temp->pre_->next_->pre_ = temp->pre_;
                temp->pre_ = temp->pre_->next_;
                temp->next_->pre_ = temp;
                flag = 0;
                continue;
            }
            temp = temp->next_;
        }
        if (flag == 1)
            break;
    }
}
#endif

void destoryList(Node *head) {
    if (NULL == head)
        return ;
    Node *tHead = head;
    while (tHead->next_ != head) {
        Node *temp = tHead->next_;
        tHead->next_ = temp->next_;
        temp->next_->pre_ = tHead;
        free(temp);
    }
    free(head);
}
