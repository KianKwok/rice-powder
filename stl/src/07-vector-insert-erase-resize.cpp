#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void printVi(vector<int> &vi);

void pushPopBackVi() {
    vector<int> vi = {1,2,3,4,5,6};
    vi.push_back(99);
    for (auto &i: vi)
        cout << i << ' ';
    putchar(10);
    while (!vi.empty()) {
        cout << vi.back() << " ";
        vi.pop_back();
    }
    cout << endl << "size = " << vi.size();
}

void insertVi1() {
    vector<int> vi = {1,2,3,4,5,6,7,8};
//    vi.insert(vi.begin(), 100);   // 100 1 2 3 4 5 6 7 8
    vi.insert(vi.begin() + 1, 200); // 1 200 2 3 4 5 6 7 8
//    vi.insert(vi.end(), 300);     // 1 2 3 4 5 6 7 8 300
    printVi(vi);
}
void insertVi2() {
    vector<int> vi = {1,2,3,4,5,6,7,8};
    vi.insert(vi.begin() + 1, 5, 999); // 1 999 999 999 999 999 2 3 4 5 6 7 8
    printVi(vi);
}
void insertVi3() {
    int arr[] = {888,888,888};
    vector<int> vi = {1,2,3,4,5,6,7,8};
    vi.insert(vi.begin() + 1, arr, arr + 2); // 1 888 888 2 3 4 5 6 7 8
//    vi.insert(vi.begin() + 1, arr, arr + 3); // 1 888 888 888 2 3 4 5 6 7 8
    printVi(vi);
}
void insertVi4() {
    vector<int> vi = {1,2,3,4,5,6,7,8};
    vi.insert(vi.begin() + 1, {777, 777, 777}); // 1 777 777 777 2 3 4 5 6 7 8
    printVi(vi);
}

void eraseVi() {
    vector<int> vi = {1,2,3,4,5,6,7,8};
//    vi.erase(vi.begin());               // 2 3 4 5 6 7 8
    vi.erase(vi.begin(), vi.begin() + 2); // 3 4 5 6 7 8
    printVi(vi);
}

void resizeVi () {
    vector<int> vi = {1,2,3,4,5,6,7,8,9};
//    vi.resize(1);   // "1 "
//    vi.resize(0);   // ""
//    vi.resize(12);  // "1 2 3 4 5 6 7 8 9 0 0 0 "
    vi.resize(12, 99);// "1 2 3 4 5 6 7 8 9 99 99 99 "
//    vi.clear();       // == vi.resize(0)  // ""
    printVi(vi);
}

void findVi() {
    vector<int> vi = {1,2,3,4,5,6,7,8};
    auto itr = find(vi.begin(), vi.end(), 1);
    if (itr == vi.end())
        cout << "Find none!" << endl;
    else {
        cout << "find it : " << *itr << endl;
        vi.erase(itr);
    }
    printVi(vi);
}

int main() {
//    pushPopBackVi();
//    insertVi1();
//    insertVi2();
//    insertVi3();
//    insertVi4();
    eraseVi();
    resizeVi();
    findVi();
    cout << "Hello World!" << endl;
    return 0;
}

void printVi(vector<int> &vi) {
    for (auto itr = vi.begin(); itr != vi.end(); ++itr) {
        cout << *itr << " ";
    }
    putchar(10);
}
