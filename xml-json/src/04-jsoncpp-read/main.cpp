#include <iostream>
#include <fstream>
#include "json/json.h"

using namespace std;
using namespace Json;

/**
{
"FirstName": "John",
"LastName": "Doe",
"Age": 43,
"Address": {
    "Street": "Downing Street 10",
    "City": "London",
    "Country": "Great Britain"
},
"Phone numbers": [
    "+44 1234567",
    "+44 2345678"
    ]
}
 */

int main() {
    fstream fs("my.json", ios::in);
    if (!fs)
        cout << "Open Error!" << endl;

    Value obj;
    Reader reader;
    if (reader.parse(fs, obj)) {
        cout << obj["FirstName"] << endl;
        cout << obj["LastName"] << endl;
        cout << obj["Age"] << endl;
        cout << obj["Age"].asInt() << endl;
        cout << obj["Address"]["Street"] << endl;
        for (int i = 0; i < obj["Phone numbers"].size(); ++i) {
            cout << obj["Phone numbers"][i] << endl;
        }
    }
    cout << "Hello World!" << endl;
    return 0;
}
