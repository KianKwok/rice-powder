#include <iostream>

using namespace std;

class Line {
public:
    virtual void drawLine() = 0;
};
class DotLine : public Line {
public:
    void drawLine() {
        cout << "This is dot line" << endl;
    }
};
class SolidLine : public Line {
public:
    void drawLine() {
        cout << "This is solid line" << endl;
    }
};
class Shape {
public:
    Shape(int x = 0, int y = 0, Line *pl = nullptr)
        :x_(x), y_(y), pl_(pl) {}
    virtual void drawShape() = 0;
protected:
    int x_;
    int y_;
    Line *pl_;
};
class Circle : public Shape {
public:
    Circle(int x = 0, int y = 0, Line *pl = nullptr, int ridus = 0)
        :Shape(x,y,pl), ridus_(ridus) {}
    void drawShape() {
        pl_->drawLine();
        cout << "Draw from point (" << x_ << ", " << y_ << "), with ridus = " << ridus_ << endl;
    }
protected:
    int ridus_;
};
class Rect : public Shape {
public:
    Rect(int x = 0, int y = 0, Line *pl = nullptr, int len = 0, int wid = 0)
        : Shape(x,y,pl), len_(len), wid_(wid) {}
    void drawShape() {
        pl_->drawLine();
        cout << "Draw from point (" << x_ << ", " << y_ << "), with len = " << len_ << " and wid = " << wid_ << endl;
    }
protected:
    int len_;
    int wid_;
};

int main() {
    DotLine dl;
    Circle c(1,2,&dl,3);
    c.drawShape();

    cout << "Hello World!" << endl;
    return 0;
}
