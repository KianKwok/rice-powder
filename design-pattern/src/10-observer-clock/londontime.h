#ifndef LONDONTIME_H
#define LONDONTIME_H


#include "observer.h"

class LondonTime : public Observer {
public:
    LondonTime();
    void uplate(int hour, int min, int sec);
};

#endif // LONDONTIME_H
