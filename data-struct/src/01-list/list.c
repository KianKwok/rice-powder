#include "list.h"
#include <stdio.h>
#include <stdlib.h>

Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->next_ = NULL;
    return head;
}
void traversalList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        printf("%d ", head->next_->data_);
        head = head->next_;
    }
    putchar(10);
}
int lenList(Node *head) {
    if (NULL == head)
        return -1;
    int len = 0;
    while (head->next_ != NULL) {
        ++len;
        head = head->next_;
    }
    return len;
}

#if 1
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;
    temp->next_ = head->next_;
    head->next_ = temp;
}
#elif 0
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;
    temp->next_ = NULL;

    while (head->next_ != NULL) {
        head = head->next_;
    }
    head->next_ = temp;
}
#else
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;

    while (head->next_ != NULL && head->next_->data_ < data) {
        head = head->next_;
    }
    temp->next_ = head->next_;
    head->next_ = temp;
}

#endif

Node * searchList(Node *head, int key) {
    if (NULL == head)
        return NULL;
    while (head->next_ != NULL) {
        if (head->next_->data_ == key) {
            return head->next_;
        }
        head = head->next_;
    }
    return NULL;
}

#if 0
void eraseList(Node *head, Node *pfind) {
    if (NULL == head || NULL == pfind)
        return ;
    while (head->next_ != NULL) {
        if (head->next_ == pfind) {
            head->next_ = pfind->next_;
            free(pfind);
        }
        head = head->next_;
    }
}
#else
// swap data. if the node is not the last one, we do not have to traversal.
void eraseList(Node *head, Node *pfind) {
    if (NULL == head || NULL == pfind)
        return ;
    if (pfind->next_ != NULL) {
        Node *temp = pfind->next_;
        pfind->data_ = temp->data_;
        pfind->next_ = temp->next_;
        free(temp);
    } else {
        while (head->next_ != pfind) {
            head = head->next_;
        }
        head->next_ = pfind->next_;
        free(pfind);
    }
}
#endif

#if 0
// swap data
void sortList(Node *head) {
    int len = lenList(head);
    for (int i = 0; i < len - 1; ++i) {
        int flag = 1;
        Node *temp = head->next_;
        for (int j = 0; j < len -i - 1; ++j) {
            if (temp->data_ > temp->next_->data_) {
                temp->data_ ^= temp->next_->data_;
                temp->next_->data_ ^= temp->data_;
                temp->data_ ^= temp->next_->data_;
                flag = 0;
            }
            temp = temp->next_;
        }
        if (flag)
            break;
    }
}
#else
// swap pointer
void sortList(Node *head) {
    int len = lenList(head);
    Node *p, *q, *pre;
    for (int i = 0; i < len - 1; ++i) {
        pre = head;
        p = head->next_;
        q = p->next_;
        for (int j = 0; j < len - i - 1; ++j) {
            if (p->data_ > q->data_) {
                pre->next_ = q;
                p->next_ = q->next_;
                q->next_ = p;

                pre = q;
                q = p->next_;
                continue;
            }
            pre = pre->next_;
            p = p->next_;
            q = q->next_;
        }
    }
}
#endif

void invertList(Node *head) {
    Node *thead = head->next_;
    head->next_ = NULL;
    while (thead) {
        Node *temp = thead->next_;
        thead->next_ = head->next_;
        head->next_ = thead;
        thead = temp;
    }
}

#if 0
void destoryList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        Node *temp = head->next_;
        head->next_ = temp->next_;
        free(temp);
    }
    free(head);
}
#else
void destoryList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        Node *temp = head->next_;
        free(head);
        head = temp;
    }
}
#endif
