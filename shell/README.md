# Shell

### Variable

* [variable](./src/01-variable.sh)
* [parameter and variable](./src/02-parameter.sh)
* [command var](./src/03-command-var.sh)
* [replace var](./src/04-replace.sh)

### Array

* [array](./src/05-array.sh)

### UI

* [echo](./src/06-echo.sh)
* [read](./src/07-read.sh)
* [printf](./src/08-printf.sh)

### Operator

* [arth](./src/09-operator-arithm.sh)
* [logic ](./src/10-operator-logic.sh)
* [file](./src/11-operator-file.sh) : Operator about file

### String

* [string definition](./src/12-string.sh)
* [string function](./src/13-string-func.sh)

### Condition

* [if](./src/14-if-condition.sh)
* [case numb](./src/15-case-condition.sh)
* [case string](./src/16-case-condition.sh)

### Loop

* [for](./src/17-for.sh)
* [while](./src/18-while.sh)
* [until](./src/19-until.sh)
* [break](./src/20-break.sh)
* [continue](./src/21-continue.sh)

### Function

* [function](./src/22-func.sh)

### Redirect

* [redirect](./src/23-redirect.sh)

### Include

* [include](./src/24-include.sh)