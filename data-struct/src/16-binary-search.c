#include <stdio.h>
#include <stdlib.h>

int binarySearch(int *arr, int low, int high, int key) {
    int mid = -1;
    while (low <= high) {
        mid = (high + low) / 2;
        if (arr[mid] == key)
            return mid;
        else if (key > arr[mid])
            low = mid + 1;
        else
            high = mid - 1;
    }
    return -1;
}
int binarySearch2(int *arr, int low, int high, int key) {
    int mid = -1;
    if (low <= high) {
        mid = (high + low) / 2;
        if (arr[mid] == key)
            return mid;
        else if (key > arr[mid])
            return binarySearch2(arr, mid + 1, high, key);
        else
            return binarySearch2(arr, low, mid - 1, key);
    }
    return -1;
}
void display(int *arr, int len) {
    for (int i = 0; i < len; ++i) {
        printf("%d ", arr[i]);
    }
    putchar(10);
}
int compare(const void *a, const void *b) {
    return (*(int *)a > *(int *)b) ? 1:0;
}
int main() {
    int arr[10] = {1,3,5,7,9,2,4,6,8,10};
    qsort((void *)arr, 10, sizeof(int), compare);
    display(arr, sizeof(arr)/sizeof(*arr));
    int find = 4;
    printf("The index of %d is %d\n", find, binarySearch2(arr, 0, 9, find));
    printf("Hello World!\n");
    return 0;
}
