#include <iostream>
#include <fstream>
#include "json/json.h"

using namespace std;
using namespace Json;

void test1() {
    Value obj;
    obj["name"] = "zhangsan";
    obj["sex"] = true;
    obj["age"] = 18;

    Value objchild;
    objchild["birth"] = "20200208";
    objchild["hobby"] = "basketball";
    obj["other"] = objchild;

    string str = obj.toStyledString();
    cout << str << endl;
}
void test2() {
    Value array;
    for (int i = 0; i < 10; ++i) {
        array.append(i);
    }

    Value obj;
    obj["k"] = "v";
    array.append(obj);

    Value array2;
    for (int i = 100; i < 110; ++i) {
        array2.append(i);
    }
    array.append(array2);

    string str = array.toStyledString();
    cout << str << endl;
}
/**
{
"FirstName": "John",
"LastName": "Doe",
"Age": 43,
"Address": {
    "Street": "Downing Street 10",
    "City": "London",
    "Country": "Great Britain"
},
"Phone numbers": [
    "+44 1234567",
    "+44 2345678"
    ]
}
 */
void test3() {
    Value obj;
    obj["FirstName"] = "John";
    obj["LastName"] = "Doe";
    obj["Age"] = 43;

    Value address;
    address["Street"] = "Downing Street 10";
    address["City"] = "London";
    address["Country"] = "Great Britain";
    obj["Address"] = address;

    Value phoneNumb;
    phoneNumb.append("+44 1234567");
    phoneNumb.append("+44 2345678");
    obj["Phone numbers"] = phoneNumb;

    string str = obj.toStyledString();
    cout << str << endl;

#if 1
    fstream fs("my.json", ios::out);
    fs << str;
    fs.close();
#elif 0
    FastWriter fw;
    string temp = fw.write(obj);
    cout << temp << endl;
#else
    StyledWriter sw;
    string temp = sw.write(obj);
    cout << temp << endl;
#endif
}

int main() {
    //    test1();
    //    test2();
    test3();
    cout << "Hello World!" << endl;
    return 0;
}
