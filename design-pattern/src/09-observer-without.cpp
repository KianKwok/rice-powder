#include <iostream>

using namespace std;

class A {
public:
    void uplate(int add) {
        x_ += add;
    }
private:
    int x_;
};
class B {
public:
    void uplate(int add) {
        x_ += add;
    }
private:
    int x_;
};
class C {
public:
    void uplate(int add) {
        x_ += add;
    }
private:
    int x_;
};

class X {
public:
    void notify(A *pa, B *pb, C *pc) {
        pa->uplate(x_ + 100);
        pb->uplate(x_ + 200);
        pc->uplate(x_ + 300);
    }
    void uplate(int add, A *pa, B *pb, C *pc) {
        x_ += 10;
        notify(pa, pb, pc);
    }
private:
    int x_;
};

int main() {
    X x;
    A a;
    B b;
    C c;

    x.uplate(10, &a, & b, &c);
    cout << "Hello World!" << endl;
    return 0;
}
