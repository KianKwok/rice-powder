#include <stdio.h>

void ShellSort(int *arr, int len) {
    int i, j, t;
    int gap = len / 2;
    while (gap >= 1) {
        for (i = gap; i < len; ++i) {
            t = arr[i];
            for (j = i; j - gap >= 0 && t <= arr[j - gap]; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = t;
        }
        gap /= 2;
    }
}
void display(int *arr, int len) {
    for (int i = 0; i < len; ++i) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int arr[] = {1,3,5,7,9,2,4,6,8};
    ShellSort(arr, sizeof(arr)/sizeof(*arr));
    display(arr, sizeof(arr)/sizeof(*arr));
    putchar(10);

    printf("Hello World!\n");
    return 0;
}
