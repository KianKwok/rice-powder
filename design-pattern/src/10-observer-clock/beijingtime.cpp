#include "beijingtime.h"

BeijingTime::BeijingTime() {

}
void BeijingTime::setTime(int hour, int min, int sec) {
    hour_ = hour;
    min_ = min;
    sec_ = sec;
    notify();
}
void BeijingTime::notify() {
    for (list<Observer *>::iterator itr = lo_.begin(); itr != lo_.end(); ++itr) {
        (*itr)->uplate(hour_, min_, sec_);
    }
}
void BeijingTime::dis() {
    cout << hour_ << ":" << min_ << ":" << sec_ << endl;
}
