#include <iostream>
#include <list>
#include <iterator>

using namespace std;

void displayL(list<int> &li);

void test_push_pop() {
    list<int> li = {1,3,5,7,9,2,4,6,8,10};

    cout << li.front() << endl;
    cout << li.back() << endl;

    li.push_back(999);
    displayL(li);
    li.push_front(888);
    displayL(li);
    li.pop_back();
    li.pop_front();
    displayL(li);

    li.insert(li.begin(), 777); // li.push_front(777);
    li.erase(li.begin());       // li.pop_front();

    displayL(li);
}
void test_remove() {
    list<int> li = {1,3,5,7,9,2,4,6,8,10,2};
    displayL(li);

    li.remove(2);
    displayL(li);

    li.remove_if([] (int x) {
        if (x > 5)
            return true;
        else
            return false;
    });
    displayL(li);
}
void test_construct() {
    int arr[] = {1,2,3,4};
    list<int> li1(arr, arr + 4);
    list<int> li2(li1.begin(), li1.end());
    displayL(li2);
}
void test_empty() {
    list<int> li = {1,3,5,7,9,2,4,6,8,10};
    cout << "size : " << li.size() << endl;
    while(!li.empty()) {
        cout << li.back() << " ";
        li.pop_back();
    }
    cout << "size : " << li.size() << endl;
}
void test_insert() {
    list<int> li = {1,2,3,4,5};
    li.insert(li.begin(), 11);
    li.insert(li.begin(), 3, 21);
    int arr[] = {31,32,33,34,35};
    li.insert(li.begin(), arr, arr + 5);
    li.insert(li.begin(), {41,42,43});
}
void test_erase() {
    list<int> li = {1,3,5,7,9};
    li.erase(li.begin());
    displayL(li);

#if 1
    li.erase(li.begin(), ++li.begin());
//    li.erase(li.begin(), li.begin() + 1); // error because list is not successive linear
#else
    list<int>::iterator itr = li.begin();
    advance(itr, 1);
    li.erase(li.begin(), itr);
#endif
    displayL(li);

    li.erase(li.begin(), li.end());
    displayL(li);
}


int main() {
//    test_push_pop();
//    test_push_pop();
//    test_construct();
//    test_empty();
    cout << endl << "Hello World!" << endl;
    return 0;
}

void displayL(list<int> &li) {
    for (auto &i:li)
        cout << i << " ";
    putchar(10);
}
