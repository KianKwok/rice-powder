#include <iostream>
#include <vector>

using namespace std;

class A {
public:
    A () {
        cout << "Parameterless constructor!       " << this << endl;
    }
    A (int data) {
        data_  = data;
        cout << "Constructor with parameter!      " << this << endl;
    }
    ~A () {
        cout << "Destructor!                      " << this << endl;
    }
    A(const A &another) {
        cout << "Copy constructor!                " << this << " = " << &another<< endl;
    }
    A & operator= (const A &another) {
        cout << "Assignment overload function!    " << this << " = " << &another << endl;
        return *this;
    }
    void setData(int data) {
        data_ = data;
    }
    int getData(void) {
        return data_;
    }
private:
    int data_;
};

void test1() {
    // memory leak occured
    vector<A *> vap;
    vap.reserve(10);
    vap.push_back(new A);
}
void test1_improve() {
    vector<A *> vap;
    vap.reserve(10);
    A *pa = new A;
    vap.push_back(pa);
    delete pa;
}


int main() {
    cout << "Hello World!" << endl;
    return 0;
}
