#include <iostream>
#include <stack>

using namespace std;

int main() {
    stack<char> sc;

    for (char ch = 'a'; ch <= 'z'; ++ch) {
        sc.push(ch);
    }

    while (!sc.empty()) {
        cout << sc.top() << " ";
        sc.pop();
    }
    putchar(10);

    cout << "Hello World!" << endl;
    return 0;
}
