#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "Cell.h"
#include <QMainWindow>
#include <QPointF>
#include <QVector>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QImage>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    ~MainWindow();

protected:
    QPointF startPoint_;
    QVector<QPointF> hLines_;
    QVector<QPointF> vLines_;

    int x_;
    int y_;

    QVector<QVector<Cell>> field_;

    void uplateSurounding(int x, int y);
    QImage image_;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
