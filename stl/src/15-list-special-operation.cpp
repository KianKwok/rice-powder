#include <iostream>
#include <list>
#include <algorithm>   // sort  ----- suited while can be accress with []
using namespace std;

void displayL(list<int> &li) {
    for (auto &i:li)
        cout << i << " ";
    putchar(10);
}

void test_sort_array() {
    int arr[] = {1,3,5,7,9,2,4,6,10};
#if 0
    sort(arr, arr + 10);
#elif 0
    cout << greater<int> () (2,3) << endl;
    sort(arr, arr + 10, greater<int>());
#else
    sort(arr, arr + 10, less<int>());
#endif
    for (auto &i:arr)
        cout << i << " ";
    putchar(10);
}
void test_sort_list() {
    list<int> li = {1,3,5,7,9,2,4,6,8,10};

//    sort(li.begin(), li.end(), less<int> ()); // error
    li.sort(less<int>());

    displayL(li);
}
void test_unique() {
    list<int> li1 = {1,2,2,3,3,3,4,4,4,4,1,2,2,3,3,3,4,4,4,4};
    li1.unique();
//    displayL(li1);

    list<int> li2 = {1,2,2,3,3,3,4,4,4,4,1,2,2,3,3,3,4,4,4,4};
    li2.sort();
    li2.unique();
//    displayL(li2);

    list<int> li3 = {1,2,2,3,3,3,4,4,4,4,1,2,2,3,3,3,4,4,4,4};
    li2.unique([](int x, int y) {
        return (x == y && y % 2);
    });
    displayL(li2);
}
void test_splice() {
    list<int> li1 = {1,3,5,7,9};
    list<int> li2 = {2,4,6,8,10};
#if 0
    li1.splice(li1.begin(), li2);
// li1 : 2 4 6 8 10 1 3 5 7 9
// li2 : ""
#elif 0
    li1.splice(li1.end(), li2, li2.begin());
// li1 : 1 3 5 7 9 2
// li2 : 4 6 8 10
#else
    auto itr = li1.begin();
    advance(itr, 2);
    li1.splice(itr, li2, li2.begin(), li2.end());
    // li1 : 1 3 2 4 6 8 10 5 7 9
    // li2 : ""
#endif
    displayL(li1);
    displayL(li2);
}
void test_reverse() {
    list<int> li1 = {1,3,5,7,9};
    li1.reverse();
    displayL(li1);
}
void test_merge() {
    list<int> li1 = {99,1,3,5,7,9};
    list<int> li2 = {2,4,6,8,10};
#if 1
    li1.merge(li2);
#else
    li1.sort();
    li2.sort();
    li1.merge(li2);
#endif
    displayL(li1);
    displayL(li2);
}

int main() {
//    test_sort_array();
//    test_sort_list();
//    test_unique();
//    test_splice();
//    test_reverse();
    test_merge();
    cout << "Hello World!" << endl;
    return 0;
}
