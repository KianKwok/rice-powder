#########################################################################
# File Name: 23-redirect.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Sat Feb 15 00:34:54 2020
#########################################################################
#!/bin/bash

echo "hello world" > file.data
cat file.data
# hello world

echo "hi world" > file.data
cat file.data
# hi world

echo "============"
echo "hello world" >> file.data
cat file.data
# hello world
# hi world

echo "============"
wc -l < file.data
# 2

echo "============"
echo "don't want to see it" > /dev/null
echo "don't want to see it or its error" > /dev/null 2>&1

rm ./file.data
