#include <iostream>
#include <memory>

using namespace std;

class A {
public:
    A() {
        cout << "A()" << endl;
    }
    ~A() {
        cout << "!A()" << endl;
    }
    void dis() {
        cout << "auto ptr" << endl;
    }
};

void func1() {
    A a;
    a.dis();
}
void func2() {
    // without destructor
    A *p = new A;
    p->dis();
}
void func3() {
    // auto destructor
    auto_ptr<A> ap(new A);
    ap->dis();
}

class SmartPointer {
public:
    SmartPointer(A *pa)
        :pa_(pa) {}
    ~SmartPointer() {
        delete pa_;
    }
    A *& operator->() { // sp->dis();
        return pa_;
    }
    A *& operator*() { // (*sp)->dis();
        return pa_;
    }
private:
    A *pa_;
};
void func4() {
    SmartPointer sp(new A);
    sp->dis();
//    (*sp)->dis();
}

int main() {
    func4();
    cout << "Hello World!" << endl;
    return 0;
}
