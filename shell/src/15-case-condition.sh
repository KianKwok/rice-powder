#########################################################################
# File Name: 15-case-condition.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 23:37:29 2020
#########################################################################
#!/bin/bash

echo -n "Input a numb (1 ~ 5) : "
read num
case ${num} in
	1) 
		echo "the numb is 1"
		;;
	2) 
		echo "the numb is 2"
		;;
	3|4)
		echo "the numb is 3 or 4"
		;;
	*)
		echo "default"
		echo "the numb is 5"
		;;
esac
