#include <iostream>

using namespace std;

enum CPUType {
    CoreA, CoreB, CoreC, CoreD
};
class CPU {
public:
    virtual void work() = 0;
};
class SingleCoreA : public CPU {
public:
    void work() {
        cout << "Single Core A working" << endl;
    }
};
class SingleCoreB : public CPU {
public:
    void work() {
        cout << "Single core B working" << endl;
    }
};
class DoubleCoreC : public CPU {
public:
    void work() {
        cout << "Double core C working" << endl;
    }
};
class DoubleCoreD : public CPU {
    void work() {
        cout << "Double core D working" << endl;
    }
};
class Factory {
public:
    virtual CPU * createCPU(enum CPUType type) = 0;
};
class FactorySingle : public Factory {
public:
    CPU * createCPU(enum CPUType type) {
        if (type == CoreA)
            return new SingleCoreA;
        else if (type == CoreB)
            return new SingleCoreB;
    }
};
class FactoryDouble : public Factory {
public:
    CPU * createCPU(enum CPUType type) {
        if (type == CoreC)
            return new DoubleCoreC;
        else if (type == CoreD)
            return new DoubleCoreD;
    }
};

int main() {
    FactorySingle fac1;
    CPU *pCPU = fac1.createCPU(CoreA);
    pCPU->work();

    FactoryDouble fac2;
    pCPU = fac2.createCPU(CoreC);
    pCPU->work();
    cout << "Hello World!" << endl;
    return 0;
}
