#include "Pos.h"

Pos::Pos(int x, int y)
    :x_(x), y_(y) {}

bool Pos::operator==(const Pos &another) {
    return (this->x_ == another.x_ && this->y_ == another.y_);
}
Pos Pos::operator+(const Pos &another) {
    Pos p;
    p.x_ = this->x_ + another.x_;
    p.y_ = this->y_ + another.y_;
    return p;
}
Pos Pos::operator-(const Pos &another) {
    Pos p;
    p.x_ = this->x_ - another.x_;
    p.y_ = this->y_ - another.y_;
    return p;
}
