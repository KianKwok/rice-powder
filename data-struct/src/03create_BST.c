#include <stdio.h>
#include <stdlib.h>

typedef struct _TreeNode {
    struct _TreeNode *right;
    struct _TreeNode *left;
    int data_;
} TreeNode;
void insertBst_recursion(TreeNode * * r, int data) {
    if (*r == NULL) {
        *r = (TreeNode *)malloc(sizeof(TreeNode));
        (*r)->data_ = data;
        (*r)->left = NULL;
        (*r)->right = NULL;
    } else if (data > (*r)->data_) {
        insertBst_recursion(&(*r)->right, data);
    } else {
        insertBst_recursion(&(*r)->left, data);
    }
}
void insertBst_loop(TreeNode ** root, int data) {
	TreeNode *temp ,*st = *root;
	if((*root) == NULL) { 
 		(*root) = (TreeNode*)malloc(sizeof(TreeNode));
		(*root)->data_ = data;
		(*root)->left = (*root)->right = NULL;
	} else {
		while(1) {
			if(data >(st)->data_) {
				if( (st)->right == NULL) {
					temp = (TreeNode*)malloc(sizeof(TreeNode));
					temp->data_ = data;
					temp->left = temp->right = NULL;
					(st)->right = temp;
					break; 
				} else
					(st) = (st)->right; 
			} else {
				if((st)->left == NULL) {
 					temp = (TreeNode*)malloc(sizeof(TreeNode));
					temp->data_ = data;
					temp->left = temp->right = NULL;
					(st)->left = temp;
					break;
				} else
					(st) = (st)->left; 
			} 
		} 
	} 
}

void MidOrder(TreeNode *r) {
    if (r) {
        MidOrder(r->left);
        printf("%d ", r->data_);
        MidOrder(r->right);
    }
}
int main() {
    TreeNode *root1 = NULL;
    insertBst_loop(&root1, 30);
    insertBst_loop(&root1, 8);
    insertBst_loop(&root1, 15);
    insertBst_loop(&root1, 36);
    insertBst_loop(&root1, 100);
    insertBst_loop(&root1, 32);
    MidOrder(root1);
    putchar(10);
    
    TreeNode *root2 = NULL;
    insertBst_recursion(&root2, 30);
    insertBst_recursion(&root2, 8);
    insertBst_recursion(&root2, 15);
    insertBst_recursion(&root2, 36);
    insertBst_recursion(&root2, 100);
    insertBst_recursion(&root2, 32);
    MidOrder(root2);
    putchar(10);
    
    printf("Hello World!\n");
    return 0;
}
