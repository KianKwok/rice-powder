#include <iostream>
#include <vector>
#include <queue>
#include <cstring>

using namespace std;

void test1() {
    priority_queue<int> pqi;
    priority_queue<int, vector<int>, greater<int>> pqi2;

    vector<int> vi = {1,3,5,7,9,2,4,6,8,10};

    for (int i = 0; i < vi.size(); ++i) {
        pqi.push(vi.at(i));
    }

    while (!pqi.empty()) {
        cout << pqi.top() << " ";
        pqi.pop();
    }
    putchar(10);
}

class Node {
public:
    Node (int priority, char *name) {
        priority_ = priority;
        strcpy(name_, name);
    }
    int priority_;
    char name_[20];
};
class NodeCmp {
public:
    bool operator() (const Node &a, const Node &b) {
        if (a.priority_ != b.priority_)
            return (a.priority_ > b.priority_);
        else
            return strcmp(a.name_, b.name_) > 0;
    }
};
void display(Node n) {
    printf("(%d, %s) ", n.priority_, n.name_);
}

void test2() {
    priority_queue<Node, vector<Node>, NodeCmp> pq;

    pq.push(Node(5, "aaa"));
    pq.push(Node(3, "bbb"));
    pq.push(Node(1, "ccc"));
    pq.push(Node(3, "ddd"));
    pq.push(Node(5, "eee"));

    while (!pq.empty()) {
        display(pq.top());
        pq.pop();
    }
    putchar(10);
}

int main() {
    test1();
    test2();
    cout << "Hello World!" << endl;
    return 0;
}
