#include <iostream>

using namespace std;

enum CPUType {
    CoreA, CoreB, CoreC
};
class CPU {
public:
    virtual void work() = 0;
};
class SingleCoreA : public CPU {
public:
    void work() {
        cout << "Core A working" << endl;
    }
};
class SingleCoreB : public CPU {
public:
    void work() {
        cout << "core B working" << endl;
    }
};
class SingleCoreC : public CPU {
public:
    void work() {
        cout << "core C working" << endl;
    }
};
class Factory {
public:
    CPU * createCPU(enum CPUType type) {
        if (type == CoreA) {
            return new SingleCoreA;
        } else if (type == CoreB) {
            return new SingleCoreB;
        } else {
            return new SingleCoreC;
        }
    }
};

int main() {
    Factory fac;
    CPU *pCPU = fac.createCPU(CoreA);
    pCPU->work();

    cout << "Hello World!" << endl;
    return 0;
}
