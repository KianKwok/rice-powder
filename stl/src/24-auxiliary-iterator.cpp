#include <iostream>
#include <list>
#include <vector>

using namespace std;

void test_plusplus_vector() {
    vector<int> vi = {1,2,3,4,5,6,7,8,9};
    auto itr = vi.begin();
    ++itr; ++itr;
    itr += 3;

    cout << *(itr + 3) << endl;
}
void test_plusplus_list() {
    list<int> li = {1,2,3,4,5,6,7,8,9};
    auto itr = li.begin();
    itr = next(itr); ++itr;
    advance(itr, 3);
    cout << *(next(itr, 3)) << endl;

    auto itr2 = next(itr, 3);
    int n = distance(itr, itr2);
    cout << " n : " << n << endl;
}

int main() {
    cout << "Hello World!" << endl;
    return 0;
}
