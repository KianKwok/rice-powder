#include <iostream>
#include <vector>
#include <time.h>

using namespace std;

void five_five_square () {
    vector<int> t(5);
    vector<vector<int>> tt(5, t);

    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            cout << (tt[i][j] = 100)<< " ";
        }
        putchar(10);
    }
}
void five_rand() {
    srand(time(nullptr));
    vector<int> t;
    vector<vector<int>> tt(5, t);

    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < rand() % 20; ++j) {
            tt[i].push_back(j);
        }
    }
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < tt[i].size(); ++j) {
            cout << tt[i][j] << " ";
        }
        putchar(10);
    }
}

int main() {
//    five_five_square();
    five_rand();
    cout << "Hello World!" << endl;
    return 0;
}
