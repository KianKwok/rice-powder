#########################################################################
# File Name: 22-func.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Sat Feb 15 00:17:35 2020
#########################################################################
#!/bin/bash

func1 () {
	echo "Hello func1"
}

#invoke function
func1
echo "return $?"

func2 () {
	echo "Hello func2"
	return 5
}
func2
echo "return $?"

echo "=============="
func3 () {
	echo "func 3 invoke func2"
	func2
}
func3

echo "=============="
func4 () {
	echo "param 1st and 2nd : $1 $2"
	echo "all param : $@"
}
func4 1 2 3
unset f func4

echo "=============="
var=666
func5 () {
	echo "Hello func5"
	var=555
}
echo "before func5, var : ${var}"
func5
echo " after func5, var : ${var}"
