#include "Snake.h"
#include <stdlib.h>

Snake::Snake() {
    SnakeNode sn;
    sn.pos_.x_ = 5;
    sn.pos_.y_ = 8;
    sn.type_ = HEAD;
    snake_.push_back(sn);

    switch (rand() % 4) {
    case 0 : dir_ = UP; break;
    case 1 : dir_ = DOWN; break;
    case 2 : dir_ = LEFT; break;
    case 3 : dir_ = RIGHT; break;
    }
}
bool Snake::isSnakeBody(const Pos &pos) {
    for (auto &item : snake_)
        if (item.pos_ == pos)
            return true;
    return false;
}
