#include <time.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
    int data;
    struct _Node *next;
} Node;

Node * createList(void);
void insertList(Node *head, int data);
void traversalList(Node *head);
int lenList(Node *head);
Node * searchList(Node *head, int find);
void deleteList1(Node *head, Node *pfind);
void deleteList2(Node *head, Node *pfind);
void bubbleSortList1(Node *head);
void bubbleSortList2(Node *head);
void reverseList(Node *head);
void destoryList1(Node *head);
void destoryList2(Node *head);

int main() {
    Node *head = createList();
    printf("Hello World!\n");

    srand((unsigned int)time(NULL));
    for (int i = 0; i < 10; ++i) {
//        insertList(head, i);
        insertList(head, rand() % 10);
    }

    traversalList(head);
//    reverseList(head);
    Node *temp = searchList(head, 2);
    if (temp)
        deleteList2(head, temp);
//    bubbleSortList1(head);
    traversalList(head);
    printf("len = %d\n", lenList(head));
    destoryList1(head);
    printf("Hello World!\n");
    return 0;
}
Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->next = NULL;
    return head;
}
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
    return ;
}
int lenList(Node *head) {
    if (NULL == head)
        return -1;
    int len = 0;
    while (head->next) {
        ++len;
        head = head->next;
    }
    return len;
}
Node * searchList(Node *head, int find) {
    if (NULL == head)
        return NULL;
    while (head->next) {
        if (head->next->data == find)
            break;
        head = head->next;
    }
    return head->next;
}
void traversalList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next) {
        printf("%d ", head->next->data);
        head = head->next;
    }
    putchar(10);
}
void deleteList1(Node *head, Node *pfind) {
    // traversal it
    if (NULL == head)
        return ;
    while (head->next) {
        if (head->next == pfind) {
            Node *temp = head->next;
            head->next = temp->next;
            free(temp);
        }
        head = head->next;
    }
}
void deleteList2(Node *head, Node *pfind) {
    // swap the next one
    if (NULL == head)
        return ;
    if (NULL == pfind->next) {
        while (head->next != pfind)
            head = head->next;
        head->next = pfind->next;
        free(pfind);
    } else {
        Node *temp = pfind->next;
        pfind->data = temp->data;
        pfind->next = temp->next;
        free(temp);
    }
}
void bubbleSortList1(Node *head) {
    // exchange data
    int len = lenList(head);
    for (int i = 0; i < len - 1; ++i) {
        int flag = 1;
        Node *temp = head->next;
        for (int j = 0; j < len - i - 1; ++j) {
            if (temp->data > temp->next->data) {
                temp->data ^= temp->next->data;
                temp->next->data ^= temp->data;
                temp->data ^= temp->next->data;
                flag = 0;
            }
            temp = temp->next;
        }
        if (flag)
            break;
    }
}
void bubbleSortList2(Node *head) {
    // exchange pointer
    int len = lenList(head);
    Node *p, *q, *pre;
    for (int i = 0; i < len - 1; ++i) {
        pre = head;
        p = head->next;
        q = p->next;
        for (int j = 0; j < len - i - 1; ++j) {
            if (p->data > q->data) {
                pre->next = q;
                p->next = q->next;
                q->next = p;

                pre = q;
                q = p->next;
                continue;

            }
            pre = pre->next;
            p = p->next;
            q = q->next;
        }
    }
}
void reverseList(Node *head) {
    Node *tHead = head->next;
    head->next = NULL;
    Node *temp = NULL;
    while (tHead) {
        temp = tHead->next;
        tHead->next = head->next;
        head->next = tHead;
        tHead = temp;
    }
}
void destoryList1(Node *head) {
    if (NULL == head)
        return ;
    Node *temp;
    while (head->next) {
        temp = head->next;
        head->next = head->next->next;
        free(temp);
    }
    free(head);
}
void destoryList2(Node *head) {
    if (NULL == head)
        return ;
    Node *temp = NULL;
    while (head) {
        temp = head->next;
        free(head);
        head = temp;
    }
}
