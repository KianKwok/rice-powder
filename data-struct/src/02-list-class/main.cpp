#include "mylist.h"
#include <time.h>
#include <iostream>

using std::cout;
using std::endl;

int main() {
    MyList mylt;

    srand(static_cast<unsigned int> (time(nullptr)));
    for (int i = 0; i < 10; ++i) {
        mylt.insert(rand() % 10);
    }
    mylt.traversal();

    mylt.invert();
    mylt.traversal();

    mylt.sort();
    mylt.traversal();

    cout << "Hello World!" << endl;
    return 0;
}
