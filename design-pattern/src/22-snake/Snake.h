#ifndef SNAKE_H
#define SNAKE_H

#include "Pos.h"
#include <list>

using std::list;

typedef enum SnakeType_ {
    HEAD, BODY, TAIL
} SnakeType;
typedef struct SnakeNode_ {
    Pos pos_;
    SnakeType type_;
} SnakeNode;
typedef enum Direct_ {
    UP, DOWN, LEFT, RIGHT
} Direct;

class Snake {
public:
    Snake();
    bool isSnakeBody(const Pos &pos);
protected:
    list<SnakeNode> snake_;
    Direct dir_;
};

#endif // SNAKE_H
