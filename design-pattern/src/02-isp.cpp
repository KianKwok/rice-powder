//Interface Segregation Principle - ISP

#include <iostream>

using namespace std;

#if 0
class IReader {
public:
    virtual string getContents(void) {};
    virtual string playBall(void) {};
};
class Book:public IReader {
public:
    string getContents(void) {
        return "Story of book";
    }
};
class Paper:public IReader {
public:
    string getContents(void) {
        return "Story of paper";
    }

};
class Mother {
public:
    void tellStory(IReader *ir) {
        cout << ir->getContents() << endl;
    }
};

class FootBall: public IReader {
public:
    string playBall(void) {
        return "Play football";
    }
};
class Father {
public:
    void play(IReader *ir) {
        cout << ir->playBall() << endl;
    }
};

#else
class IReader {
public:
    virtual string getContents(void) {};
};
class Book:public IReader {
public:
    string getContents(void) {
        return "Story of book";
    }
};
class Paper:public IReader {
public:
    string getContents(void) {
        return "Story of paper";
    }

};
class Mother {
public:
    void tellStory(IReader *ir) {
        cout << ir->getContents() << endl;
    }
};

class Ball {
public:
    virtual string playBall(void) {}
};
class FootBall: public Ball {
public:
    string playBall(void) {
        return "Play football";
    }
};
class Father {
public:
    void play(Ball *ir) {
        cout << ir->playBall() << endl;
    }
};

#endif

int main() {
    Book b;
    Paper p;
    Mother m;
    m.tellStory(&b);
    m.tellStory(&p);

    FootBall fb;
    Father f;
    f.play(&fb);
    cout << "Hello World!" << endl;
    return 0;
}
