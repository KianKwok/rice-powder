#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
    int data_;
    struct _Node *next_;
} Node;

Node * createList(void);
void traversalList(Node *head);
void insertList(Node *head, int data);
void destoryList(Node *head);

Node * midNode(Node *head) {
    if (NULL == head)
        return NULL;
    if (NULL == head->next_)
        return NULL;
    if ( NULL == head->next_->next_)
        return head->next_;
    Node *temp1 = head->next_;
    Node *temp2 = head->next_->next_;
    while (1) {
        if (NULL == temp2->next_)
            return temp1;
        temp2 = temp2->next_;
        if (NULL == temp2->next_)
            return temp1->next_;
        temp2 = temp2->next_;
        temp1 = temp1->next_;
    }
}

int main() {
    Node *head = createList();

    for (int i = 0; i < 5; ++i) {
        insertList(head, i + 1);
    }

    traversalList(head);
    Node *mid = midNode(head);
    if (NULL != mid)
        printf("%d\n", mid->data_);
    destoryList(head);
    printf("Hello World!\n");
    return 0;
}

Node * createList(void) {
    Node *head = (Node *)malloc(sizeof(Node));
    if (NULL == head)
        return NULL;
    head->next_ = NULL;
    return head;
}
void traversalList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        printf("%d ", head->next_->data_);
        head = head->next_;
    }
    putchar(10);
}
void insertList(Node *head, int data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    if (NULL == temp)
        return ;
    temp->data_ = data;
    temp->next_ = head->next_;
    head->next_ = temp;
}
void destoryList(Node *head) {
    if (NULL == head)
        return ;
    while (head->next_ != NULL) {
        Node *temp = head->next_;
        free(head);
        head = temp;
    }
}
