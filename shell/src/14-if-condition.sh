#########################################################################
# File Name: 14-if-condition.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 23:33:21 2020
#########################################################################
#!/bin/bash

a=10
b=20
if [ ${a} == ${b} ]
then
	echo "a == b"
else
	echo "a != b"
fi

c=30
if [ ${a} == ${b} ]
then
	echo "a == b"
elif [ ${a} == ${c} ]
	echo "a == c"
else
	echo "a != b && a != c"
fi

if [ ${a} == 10 ]; then echo "a == 10"; fi;
