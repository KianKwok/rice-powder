#include <stdio.h>
#include <stdlib.h>

typedef struct _TreeNode {
    struct _TreeNode *right_;
    struct _TreeNode *left_;
    int data_;
} TreeNode;

void MidOrder(TreeNode *r);
void insertBst_recursion(TreeNode * * r, int data);

TreeNode * GetMinNode(TreeNode *r) {
    if (r) {
        while (r->left_) {
            r = r->left_;
        }
        return r;
    }
    return NULL;
}
TreeNode * GetMaxNode(TreeNode *r) {
    if (r) {
        while (r->right_) {
            r = r->right_;
        }
        return r;
    }
    return NULL;
}

int main() {
    TreeNode *root = NULL;
    insertBst_recursion(&root, 30);
    insertBst_recursion(&root, 8);
    insertBst_recursion(&root, 15);
    insertBst_recursion(&root, 36);
    insertBst_recursion(&root, 100);
    insertBst_recursion(&root, 32);
    MidOrder(root);
    putchar(10);

    TreeNode *pMin = GetMinNode(root);
    if (pMin != NULL)
        printf("The smallest node is %d\n", pMin->data_);
    else
        printf("The tree is NULL\n");

    TreeNode *pMax = GetMaxNode(root);
    if (pMax != NULL)
        printf("The biggest node is %d\n", pMax->data_);
    else
        printf("The tree is NULL\n");

    printf("Hello World!\n");
    return 0;
}

void MidOrder(TreeNode *r) {
    if (r) {
        MidOrder(r->left_);
        printf("%d ", r->data_);
        MidOrder(r->right_);
    }
}
void insertBst_recursion(TreeNode * * r, int data) {
    if (*r == NULL) {
        *r = (TreeNode *)malloc(sizeof(TreeNode));
        (*r)->data_ = data;
        (*r)->left_ = NULL;
        (*r)->right_ = NULL;
    } else if (data > (*r)->data_) {
        insertBst_recursion(&(*r)->right_, data);
    } else {
        insertBst_recursion(&(*r)->left_, data);
    }
}
