#include <iostream>
#include <vector>

using namespace std;

void mysize() {
    vector<int> vi = {4,5,6};
    cout << "size = " << vi.size() << endl;
    cout << "max size = " << vi.max_size() << endl;
}
void reserveAndCapa() {
    putchar(10);
    cout << "reserve and capacity " << endl;
    vector<int> vi;
    vi.reserve(10);
    cout << "size     = " << vi.size() << endl;
    cout << "capacity = " << vi.capacity() << endl;
    vi.push_back(99);
    cout << "size     = " << vi.size() << endl;
    cout << "capacity = " << vi.capacity() << endl;
    vi.shrink_to_fit();
    cout << "size     = " << vi.size() << endl;
    cout << "capacity = " << vi.capacity() << endl;
}
void cmp() {
    // The same as string compare
    vector<int> vi1 = {1,2,3,4,5};
    vector<int> vi2 = {1,2,3,5,4};

    if (vi1 == vi2)
        cout << " == " << endl;
    else {
        cout << " != " << endl;
        if (vi1 > vi2)
            cout << " > " << endl;
        else
            cout << " < " << endl;
    }
}
int main() {
    mysize();
    reserveAndCapa();
    cmp();

    putchar(10);
    cout << "Hello World!" << endl;
    return 0;
}
