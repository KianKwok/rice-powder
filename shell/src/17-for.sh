#########################################################################
# File Name: 17-for.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Sat Feb 15 00:00:14 2020
#########################################################################
#!/bin/bash

for var in 1 2 3 4 5
do
	echo "The value is : ${var}"
done

for FILE in $HOME/.bash*
do
	echo $FILE
done
