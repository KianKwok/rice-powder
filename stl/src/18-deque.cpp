#include <iostream>
#include <deque>

using namespace std;

// container adapter

void test_stack() {
    deque<char> dc;

    for (char ch = 'a'; ch <= 'z'; ++ch)
        dc.push_front(ch);

    while (!dc.empty()) {
        cout << dc.front() << " ";
        dc.pop_front();
    }
    putchar(10);
}
void test_queue() {
    deque<char> dc;

    for (char ch = 'a'; ch <= 'z'; ++ch)
        dc.push_front(ch);

    while (!dc.empty()) {
        cout << dc.back() << " ";
        dc.pop_back();
    }
    putchar(10);
}

int main() {
    test_stack();
    test_queue();
    cout << "Hello World!" << endl;
    return 0;
}
