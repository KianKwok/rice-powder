#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAXROW 10
#define MAXCOL 10

typedef struct _Point {
    int x_;
    int y_;
} Point;
typedef struct _Node {
    Point data;
    struct _Node *next;
} Node;
typedef struct _Stack {
    Node *top;
} Stack;

void initStack(Stack *s);
int isEmptyStack(Stack *s);
void pushStack(Stack *s, Point data);
Point popStack(Stack *s);
void resetStack(Stack *s);
void destoryStack(Stack *s);

void displayMaze(int maze[][MAXCOL]);
void visit(Stack *s, int x, int y) {
    Point p = {x, y};
    pushStack(s, p);
}

int main() {
    int maze[MAXROW][MAXCOL] = {
        {1,1,1,1,1,1,1,1,1,1},
        {0,0,0,1,1,1,1,1,1,1},
        {1,1,0,1,1,1,1,1,1,1},
        {1,1,0,0,0,0,1,1,1,1},
        {1,1,0,1,1,0,1,1,1,1},
        {1,1,0,1,1,0,1,1,1,1},
        {1,1,1,1,1,0,1,1,1,1},
        {1,1,1,1,1,0,0,0,1,1},
        {1,1,1,1,1,1,1,0,0,0},
        {1,1,1,1,1,1,1,1,1,1},
    };
    Stack s;

    Point sp = {1, 0};
    Point ep = {8, 9};

    initStack(&s);
    pushStack(&s, sp);

    int flag = 1;

    while (!isEmptyStack(&s)) {
        Point t;
        t = popStack(&s);
        maze[t.x_][t.y_] = 2;

        system("cls");
        displayMaze(maze);
        sleep(1);

        if (t.x_ - 1 >= 0 && maze[t.x_ - 1][t.y_] != 1 && maze[t.x_ - 1][t.y_] != 2) {
            visit(&s, t.x_ - 1, t.y_);
        }
        if (t.x_ + 1 < MAXROW && maze[t.x_ + 1][t.y_] != 1 && maze[t.x_ + 1][t.y_] != 2) {
            visit(&s, t.x_ + 1, t.y_);
        }
        if (t.y_ - 1 >= 0 && maze[t.x_][t.y_ - 1] != 1 && maze[t.x_][t.y_ - 1] != 2) {
            visit(&s, t.x_, t.y_ - 1);
        }
        if (t.y_ + 1 < MAXCOL && maze[t.x_][t.y_ + 1] != 1 && maze[t.x_][t.y_ + 1] != 2) {
            visit(&s, t.x_, t.y_ + 1);
        }

        if (t.x_ == ep.x_ && t.y_ == ep.y_) {
            flag = 0;
            break;
        }
    }
    if (flag == 0)
        printf("find path!\n");
    else
        printf("No path!\n");

    destoryStack(&s);
    printf("Hello World!\n");
    return 0;
}
void displayMaze(int maze[][MAXCOL]) {
    for (int i = 0; i < MAXROW; ++i) {
        for (int j = 0; j < MAXCOL; ++j) {
            if (maze[i][j] == 1)
                printf("%2s", " *");
            else if (maze[i][j] == 2)
                printf("%2s", " #");
            else
                printf("%2s", "  ");
        }
        putchar(10);
    }
    printf("====================\n");
}
void initStack(Stack *s) {
    s->top = (Node *)malloc(sizeof(Node));
    s->top->next = NULL;
}
int isEmptyStack(Stack *s) {
    return (s->top->next == NULL);
}
void pushStack(Stack *s, Point data) {
    Node *temp = (Node *)malloc(sizeof(Node));
    temp->data = data;
    temp->next = s->top->next;
    s->top->next = temp;
}
Point popStack(Stack *s) {
    Node *temp = s->top->next;
    Point ch = temp->data;
    s->top->next = temp->next;
    free(temp);
    return ch;
}
void resetStack(Stack *s) {
    while (!isEmptyStack(s)) {
        popStack(s);
    }
}
void destoryStack(Stack *s) {
    resetStack(s);
    free(s->top);
}
