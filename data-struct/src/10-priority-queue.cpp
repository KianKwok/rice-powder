#include <iostream>
#include <queue>

using namespace std;

int main() {
    int arr[10] = {1,3,5,7,9,2,4,6,8,10};
    priority_queue<int> pq;

    pq.push(100);
    pq.push(80);
    pq.push(100);

    for (int i = 0; i < 10; ++i) {
        pq.push(arr[i]);
    }

    while (!pq.empty()) {
        cout << pq.top() << " ";
        pq.pop();
    }
    putchar(10);
    cout << "Hello World!" << endl;
    return 0;
}
