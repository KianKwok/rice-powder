#########################################################################
# File Name: 05-array.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Wed Feb 12 21:18:12 2020
#########################################################################
#!/bin/bash

arr=(1 2 3 4)
arr[1]=5
echo "arr[1] : ${arr[1]}"
echo "arr[1] : $arr[1]"

echo "====================="
echo "arr[*] : ${arr[*]}"
#echo "arr[@] : ${arr[@]}"
echo "numb of arr elem : ${#arr[*]}"

echo "====================="
arr2[2]=10
echo "numb of arr2 elem : ${#arr2[@]}"

echo "====================="
arr3[2]="hello"
echo "numb of arr3 elem : ${#arr3[@]}"
echo "size of arr3[2]   : ${#arr3[2]}"
