#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <cctype>

using namespace std;

int main() {
    vector<int> vi = {1,2,3,1,2,3,3,4,5,4,5,6,7};

    sort(vi.begin(), vi.end());    // 1 1 2 2 3 3 3 4 4 5 5 6 7
    auto last = unique(vi.begin(), vi.end()); // 1 2 3 4 5 6 7 x x x x x x --- 'x' is indeterminate
    vi.erase(last, vi.end());

    for (int &i:vi)
        cout << i << " ";
    putchar(10);

    // remove consecutive spaces
    string s = "wanna go    to    space?";
    auto end = unique(s.begin(), s.end(), [](char l, char r){
        return isspace(l) && isspace(r) && l == r;
    });
    cout << string(s.begin(), end) << endl; // wanna go to space?xxxxxxxx --- 'x' is indeterminate

    return 0;
}

