#########################################################################
# File Name: 11-operator-file.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 23:24:03 2020
#########################################################################
#!/bin/bash

File=$1

if [ ‐r $File ]
then
	echo "$File has read permission"
else
	echo "$File not have read permission"
fi

if [ ‐w $File ]
then
	echo "$File has write permission"
else
	echo "$File not have write permission"
fi

if [ ‐x $File ]
then
	echo "$File has execute permission"
else
	echo "$File not have execute permission"
fi

if [ ‐f $File ]
then
	echo "$File is regular file"
else
	echo "$File is special file"
fi

if [ ‐d $File ]
then
	echo "$File is directory"
else
	echo "$File is not directory"
fi

if [ ‐s $File ]
then
	echo "$File size is not zero"
else
	echo "$File size is zero"
fi

if [ ‐e $File ]
then
	echo "$File exist"
else
	echo "$File not exist"
fi