#include <iostream>
#include <unistd.h>

using namespace std;

class Text {
public:
    void showText() {
        cout << "fine fine fine fine fine" << endl;
        cout << "fine fine fine fine fine" << endl;
        cout << "fine fine fine fine fine" << endl;
        cout << "fine fine fine fine fine" << endl;
        cout << "fine fine fine fine fine" << endl;
    }
};
class ImageSubject {
public:
    virtual void showImage() = 0;
};

class BigImage : ImageSubject {
public:
    BigImage() {
        sleep(6);
        cout << "image load finish" << endl;
    }
    void showImage() {
        cout << "show image" << endl;
    }

};
class ProxyImage : public ImageSubject {
public:
    ProxyImage() {
        bi = nullptr;
    }
    void showImage() {
        if (nullptr == bi)
            bi = new BigImage;
        bi->showImage();
    }
private:
    BigImage *bi;
};

#if 0
// without proxy
class Doc {
public:
    Doc() {
        t = new Text;
        bi = new BigImage;
    }
    void show() {
        t->showText();
        bi->showImage();
    }
private:
    Text *t;
    BigImage *bi;
};
#else
// with proxy
class Doc {
public:
    Doc() {
        t = new Text;
        pi = new ProxyImage;
    }
    void show() {
        t->showText();
        pi->showImage();
    }
private:
    Text *t;
    ProxyImage *pi;
};

#endif

int main() {
    Doc d;
    d.show();
    cout << "Hello World!" << endl;
    return 0;
}
