#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

bool func(int &n) {
    return (n >= 3) ? true : false;
}

void test_array() {
    int data[] = {1,2,3,4,5};
    int *p = find(data, data + 5, 4);
    if (p != data + 5)
        cout << "Find " << *p << endl;
    else
        cout << "Find none" << endl;
}
void test_vector() {
    vector<int> vi = {1,2,3,4,5};
    vector<int>::iterator itr = find(vi.begin(), vi.end(), 10);

    if (itr != vi.end())
        cout << "find " << *itr << endl;
    else
        cout << "find none" << endl;

    itr = find_if(vi.begin(), vi.end(), func);

    if (itr != vi.end())
        cout << "find " << *itr << endl;
    else
        cout << "find none" << endl;
}

int main() {

    cout << "Hello World!" << endl;
    return 0;
}
