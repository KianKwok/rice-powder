# C Improvement

## Catalog

DataType

* [Date Type](./src/01-format.c)
* [Typecast](./src/02-typecast.c)
* [Display Binary](./src/03-dis-bin.c)

Program Space

* [Process Space](./src/04-process-space.c)
: Describe the location of the data in the process space.
* [Function Call](./src/05-function-call.c)
: The generation and disappearance of data during function calls.
* [Stack Data](./src/06-stack-data.c)
: Use stack to display array in order or in reverse-order.
* [Display Complement](./src/07-display-complement.c)
: Dispaly the binary code of the complement of a negative.
* [Exercise](./src/08-exercise.c)
: A typical exercise question about binary code.

Array

Pointer

Function

Pointer and Array

Sort

Search

String

List

Library

File

SQLite

Generic
