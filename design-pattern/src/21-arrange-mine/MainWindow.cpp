#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QPainter>
#include <QDebug>
#include <QImage>

#define WIDGET_W 600
#define WIDGET_H 400
//#define FIELD_W 600
//#define FIELD_H 400
#define VX_LINES 16
#define HY_LINES 11
#define CELL_W_H 35

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    this->setGeometry(500, 300, WIDGET_W, WIDGET_H);
    startPoint_.setX((WIDGET_W - CELL_W_H * (VX_LINES - 1)) / 2);
    startPoint_.setY((WIDGET_H - CELL_W_H * (HY_LINES - 1)) / 2);

    for (int i = 0; i < VX_LINES; ++i) {
        hLines_.push_back(QPointF(startPoint_.x() + CELL_W_H * i, startPoint_.y()));
        hLines_.push_back(QPointF(startPoint_.x() + CELL_W_H * i, startPoint_.y() + CELL_W_H * (HY_LINES - 1)));
    }
    for (int i = 0; i < HY_LINES; ++i) {
        vLines_.push_back(QPointF(startPoint_.x(), startPoint_.y() + CELL_W_H * i));
        vLines_.push_back(QPointF(startPoint_.x() + CELL_W_H * (VX_LINES - 1), startPoint_.y() + CELL_W_H * i));
    }
    field_.reserve(15);
    for (int i = 0; i < VX_LINES - 1; ++i) {
        field_.push_back(QVector<Cell>(HY_LINES - 1));
    }
    image_.load(":/pic/mine.jpg");
    image_ = image_.scaled(QSize(CELL_W_H, CELL_W_H));
}

void MainWindow::paintEvent(QPaintEvent *event) {
    QPainter painter;
    painter.begin(this);

    for (int i = 0; i < VX_LINES - 1; ++i) {
        for (int j = 0; j < HY_LINES - 1; ++j) {
            switch (field_[i][j].getStatus()) {
            case BLANK:
            case DIGIT: {
                QPointF temp = startPoint_ + QPointF(i * CELL_W_H, j * CELL_W_H);
                painter.drawText(temp.x() + 15, temp.y() + 25, QString::number(field_[i][j].getMine()));
                break;
            }
            case MINE: {
                painter.drawImage(QPointF(startPoint_ + QPointF(i * CELL_W_H, j * CELL_W_H)), image_);
                break;
            }
            default:
                break;
            }
        }
    }

    painter.drawLines(hLines_);
    painter.drawLines(vLines_);

    painter.end();
}
void MainWindow::mousePressEvent(QMouseEvent *event) {
    if (startPoint_.x() < event->x() && event->x() < WIDGET_W - startPoint_.x()
     && startPoint_.y() < event->y() && event->y() < WIDGET_H - startPoint_.y()) {
        QPoint pos = event->pos();
        x_ = (pos.x() - startPoint_.x()) / CELL_W_H;
        y_ = (pos.y() - startPoint_.y()) / CELL_W_H;

        if (field_[x_][y_].getStatus() != MINE) {
            field_[x_][y_].setMine(-1);
            field_[x_][y_].setStatus(MINE);
            uplateSurounding(x_ - 1, y_ - 1);
            uplateSurounding(x_ - 1, y_    );
            uplateSurounding(x_ - 1, y_ + 1);
            uplateSurounding(x_    , y_ - 1);
            uplateSurounding(x_    , y_ + 1);
            uplateSurounding(x_ + 1, y_ - 1);
            uplateSurounding(x_ + 1, y_    );
            uplateSurounding(x_ + 1, y_ + 1);
        }
    }
    update();
}

void MainWindow::uplateSurounding(int x, int y) {
    if (0 <= x && x < VX_LINES - 1 && 0 <= y && y < HY_LINES - 1) {
        if (field_[x][y].getStatus() != MINE) {
            int temp = field_[x][y].getMine();
            field_[x][y].setMine(++temp);
            field_[x][y].setStatus(DIGIT);
        }
    }
}

MainWindow::~MainWindow() {
    delete ui;
}
