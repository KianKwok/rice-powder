#include <iostream>

using namespace std;

#if 0
// function
void func2() {

}
void func1() {
    func2();
}

#else
class B {
public:
    void func2() {

    }
};
class A {
public:
    void func1(B &rb, B *pb, B b) {
       rb_.func2();
       pb_->func2();
       b_.func2();

       rb.func2();
       pb->func2();
       b.func2();
    }
private:
    B &rb_;
    B *pb_;
    B b_;
};

#endif

int main() {
    cout << "Hello World!" << endl;
    return 0;
}
