#########################################################################
# File Name: 04-replace.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Wed Feb 12 22:01:54 2020
#########################################################################
#!/bin/bash

#var1="hello"

echo "----------------------"
echo "\$var1 : ${var1}"
echo "\${var1:-'world1'} : ${var1:-'world1'}"
echo "\$var1 : ${var1}"

echo "====================="
echo "\$var1 : ${var1}"
echo "\${var1:='world2'} : ${var1:='world2'}"
echo "\$var1 : ${var1}"

echo "++++++++++++++++++++"
echo "\$var1 : ${var1}"
echo "\${var1:+'world3'} : ${var1:+'world3'}"
echo "\$var1 : ${var1}"

echo "???????????????????"
unset var1
echo "\$var1 : ${var1}"
echo "\${var1:?'world4'} : ${var1:?'var1 is empty'}"
echo "\$var1 : ${var1}"
