#include <iostream>
#include <set>

using namespace std;

void test() {
    set<string, greater<string>> ss;
//    multiset<string> ss;
    ss.insert("aaa");
    ss.insert("bbb");
    ss.insert("ccc");
    ss.insert("ddd");
    ss.insert("ddd");

    for (auto &s: ss)
        cout << s << " ";
    putchar(10);
}

int main() {
    test();
    cout << "Hello World!" << endl;
    return 0;
}
