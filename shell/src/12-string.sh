#########################################################################
# File Name: 12-string.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Fri Feb 14 23:06:35 2020
#########################################################################
#!/bin/bash

echo 'this is a \nstring'
#this is a \nstring

echo "this is a \nstring"
#this is a 
#string

str="coffee"
echo 'this is ${str}'
# this is ${str}
echo "this is ${str}"
# this is coffee

str2="tonight"
str3=${str}${str2}
echo ${str3}
#coffeetonight
