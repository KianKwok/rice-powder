#ifndef MYLIST_H
#define MYLIST_H

#include <iostream>

using std::cout;

template <typename T>
class Node {
public:
    T data_;
    Node<T> *next_;
};

template <typename T>
class MyList {
public:
    MyList();
    ~MyList();
    void traversal();
    int len();
    void insert(T data);
    Node<T> * search(T key);
    void erase(Node<T> *pfind);
    void sort();
    void invert();

private:
    int len_;
    Node<T> *head_;
};

template <typename T>
MyList<T>::MyList() {
    head_ = new Node<T>;
    head_->next_ = nullptr;
    len_ = 0;
}
template <typename T>
MyList<T>::~MyList() {
    while (head_->next_ != nullptr) {
        Node<T> *temp = head_->next_;
        head_->next_ = temp->next_;
        delete temp;
    }
    delete head_;
}
template <typename T>
void MyList<T>::traversal() {
    Node<T> *tHead = head_;
    while (tHead->next_ != nullptr) {
        cout << tHead->next_->data_ << " ";
        tHead = tHead->next_;
    }
    putchar(10);
}
template <typename T>
int MyList<T>::len() {
    return len_;
}
template <typename T>
void MyList<T>::insert(T data) {
    Node<T> *temp = new Node<T>;
    temp->data_ = data;
    temp->next_ = head_->next_;
    head_->next_ = temp;
    ++len_;
}
template <typename T>
Node<T> * MyList<T>::search(T key) {
    Node<T> *tHead = head_;
    while (tHead->next_ != nullptr) {
        if (tHead->next_->data_ == key)
            return tHead->next_;
        tHead = tHead->next_;
    }
    return nullptr;
}
template <typename T>
void MyList<T>::erase(Node<T> *pfind) {
    if (nullptr == pfind)
        return ;
    if (pfind->next_ != nullptr) {
        Node<T> *temp = pfind->next_;
        pfind->data_ = temp->data_;
        pfind->next_ = temp->next_;
        delete temp;
        --len_;
    } else {
        Node<T> *tHead = head_;
        while (tHead->next_ != nullptr) {
            if (tHead->next_ == pfind) {
                tHead->next_ = pfind->next_;
                delete pfind;
                --len_;
                return ;
            }
            tHead = tHead->next_;
        }
    }
}
template <typename T>
void MyList<T>::sort() {
    Node<T> *p, *q, *pre;
    for (int i = 0; i < len_ - 1; ++i) {
        pre = head_;
        p = head_->next_;
        q = p->next_;
        for (int j = 0; j < len_ - i - 1; ++j) {
            if (p->data_ > q->data_) {
                pre->next_ = q;
                p->next_ = q->next_;
                q->next_ = p;

                pre = q;
                q = p->next_;
                continue;
            }
            pre = pre->next_;
            p = p->next_;
            q = q->next_;
        }
    }
}
template <typename T>
void MyList<T>::invert() {
    Node<T> *thead = head_->next_;
    head_->next_ = nullptr;
    while (thead) {
        Node<T> *temp = thead->next_;
        thead->next_ = head_->next_;
        head_->next_ = thead;
        thead = temp;
    }
}

#endif // MYLIST_H
