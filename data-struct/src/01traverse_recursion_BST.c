#include <stdio.h>

typedef struct _TreeNode {
    struct _TreeNode *right_;
    struct _TreeNode *left_;
    int data_;
} TreeNode;

void PreOrder(TreeNode *r) {
	if (r) {
        printf("%d ", r->data_);
        PreOrder(r->left_);
        PreOrder(r->right_);
	}
}
void MidOrder(TreeNode *r) {
	if (r) {
        MidOrder(r->left_);
        printf("%d ", r->data_);
        MidOrder(r->right_);
	}
}
void PostOrder(TreeNode *r) {
	if (r) {
        PostOrder(r->left_);
        PostOrder(r->right_);
        printf("%d ", r->data_);
	}
}

int main() {
    TreeNode a, b, c, d, e, f;
	TreeNode *root = &a;

	a.data_ = 1;
	b.data_ = 2;
	c.data_ = 3;
	d.data_ = 4;
	e.data_ = 5;
	f.data_ = 6;

	a.left_ = &b; a.right_ = &c;
	b.left_ = &d; b.right_ = NULL;
	c.left_ = &f; c.right_ = NULL;
	d.left_ = d.right_ = NULL;
	e.left_ = e.right_ = NULL;
	f.left_ = f.right_ = NULL;

    PreOrder(root);
	putchar(10);
	MidOrder(root);
	putchar(10);
	PostOrder(root);
	putchar(10);

    printf("Hello World!\n");
    return 0;
}
