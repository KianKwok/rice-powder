#ifndef CONFIG_H
#define CONFIG_H


#include <iostream>

using namespace std;

struct Infor {
    string ip_;
    string port_;
};

class Config
{
public:
    static Config * getInstance();
    string getIp();
    string getPort();
private:
    static Config *ins_;
    Config();
    ~Config() {}
    struct Infor infor;
};

#endif // CONFIG_H
