#include <iostream>
#include <vector>

using namespace std;

void inefficient_reserve() {
    vector<int> vi;
    for (int i = 0; i < 10; ++i) {
        vi.push_back(i);
        cout << "size : " << vi.size() << " ; ";
        cout << "capa : " << vi.capacity() << endl;// realloc
    }
// Low efficiency due to multiple space applications
}
void better_reserve() {
    vector<int> vi;
    vi.reserve(16);   //
    for (int i = 0; i < 10; ++i) {
        vi.push_back(i);
        cout << "size : " << vi.size() << " ; ";
        cout << "capa : " << vi.capacity() << endl;// realloc
    }
    vi.shrink_to_fit();
    for (int i = 0; i < 10; ++i) {
        vi.push_back(i);
        cout << "size : " << vi.size() << " ; ";
        cout << "capa : " << vi.capacity() << endl;// realloc
    }
}
void resize_exam() {
    vector<int> vi = {1,2,3,4,5};
    cout << vi.capacity() << endl;   // capa = 5
    vi.resize(10);
    cout << vi.capacity() << endl;   // capa = 10
    vi.resize(1);
    cout << vi.capacity() << endl;   // capa = 10
}

int main() {
        cout << "Hello World!" << endl;
    return 0;
}
