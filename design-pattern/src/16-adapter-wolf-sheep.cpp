#include <iostream>

using namespace std;

class Sheep {
public:
    virtual void BaaBaa() = 0;
};
class NormalSheep : public Sheep {
public:
    void BaaBaa() {
        cout << "I am a real sheep" << endl;
    }
};
class Wolf {
public:
    void howl() {
        cout << "I am a real wolf" << endl;
    }
};
class AdapterSheep : public Sheep {
public:
    AdapterSheep(Wolf *w)
        :w_(w) {}
    void BaaBaa() {
        cout << "====" << endl;
        cout << "I am a wolf in sheep's clothing" << endl;
        w_->howl();
        cout << "====" << endl;
    }
protected:
    Wolf *w_;
};

int main() {
//    NormalSheep ns;
//    ns.BaaBaa();

    Wolf w;
//    w.howl();

    AdapterSheep as(&w);
    as.BaaBaa();

    cout << "Hello World!" << endl;
    return 0;
}
