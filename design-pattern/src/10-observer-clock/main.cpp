#include <iostream>
#include "beijingtime.h"
#include "canadatime.h"
#include "londontime.h"
#include "tokyotime.h"

using namespace std;

int main() {
    TokyoTime tt;
    LondonTime lt;
    CanadaTime ct;

    BeijingTime bt;
    bt.registerObserver(&tt);
    bt.registerObserver(&lt);
    bt.registerObserver(&ct);

    bt.setTime(100,101,102);
    bt.dis();
    cout << "======================" << endl;

    bt.removeObserver(&tt);
    bt.setTime(500,501,502);
    bt.dis();

    cout << "Hello World!" << endl;
    return 0;
}
