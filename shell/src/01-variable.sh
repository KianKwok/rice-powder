#########################################################################
# File Name: 01-variable.sh
# Author: Hing
# Mail: hing123@126.com
# Created Time: Wed Feb 12 18:17:01 2020
#########################################################################
#!/bin/bash

echo "hello world"
echo "HELLO WORLD"

echo "===================="
var1=10
var2="Hello"

echo "$var1"
echo "$var2 My"
echo "${var2} My"

echo "===================="
var3=15
#readonly var3
var3=20
echo "${var3}"

echo "=================="
var4=25
unset var4
echo "${var4}"

#export var_env="env variable"
