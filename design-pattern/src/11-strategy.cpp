#include <iostream>

using namespace std;

class Weapon {
public:
    virtual void use();
};
class Knife : public Weapon {
public:
    void use() {
        cout << "use knife" << endl;
    }
};
class Gun : public Weapon {
public:
    void use() {
        cout << "use gun" << endl;
    }
};
class Sprite {
public:
    Sprite(Weapon *wp) {
        wp_ = wp;
    }
    void setWeapon(Weapon *wp) {
        wp_ = wp;
    }
    void fight() {
        wp_->use();
    }
private:
    Weapon *wp_;
};

int main() {
    Knife k;
    Gun g;
    Sprite sp(&k);
    sp.fight();
    sp.setWeapon(&g);
    sp.fight();
    cout << "Hello World!" << endl;
    return 0;
}
