#include <iostream>
#include "tinyxml2.h"

using namespace std;
using namespace tinyxml2;

/**
<?xml version="1.0" encoding="utf-8"?>
<!-- This is a XML comment -->
<bookstore>
    <book category="COOKING">
        <title lang="en" id="123456" pages="123">Everyday Italian</title>
        <author>Giada De Laurentiis</author>
        <year>2005</year>
        <price>30.00</price>
    </book>
    <book category="CHILDREN">
        <title lang="en">Harry Potter</title>
        <author>J K. Rowling</author>
        <year>2005</year>
        <price>29.99</price>
    </book>
    <book category="WEB">
        <title lang="en">Learning XML</title>
        <author>Erik T. Ray</author>
        <year>2003</year>
        <price>39.95</price>
    </book>
    <book/>
</bookstore>
 */
int main() {
    XMLDocument *doc = new XMLDocument;
    doc->LoadFile("xxx.xml");

    XMLElement *pebookstore = doc->FirstChildElement();
    cout << pebookstore->Name() << endl;

    auto pebook = pebookstore->FirstChildElement();
    cout << pebook->Name() << endl;

    cout << pebook->FirstAttribute()->Name() << ":";
    cout << pebook->FirstAttribute()->Value() << endl;

    auto petitle = pebook->FirstChildElement();
    while (petitle) {
        cout << petitle->Name() << ":" << petitle->GetText() << endl;
        auto peTitleAtt = petitle->FirstAttribute();
        while (peTitleAtt) {
            cout << peTitleAtt->Name() << ":" << peTitleAtt->Value() << endl;
            peTitleAtt = peTitleAtt->Next();
        }
        petitle = petitle->NextSiblingElement();
    }

    cout << "========" << endl << endl;
    doc->Print();

    cout << "========" << endl << endl;

    XMLPrinter printer;
    doc->Print(&printer);
    cout << "XML : " << printer.CStr() << endl;
    cout << "XML Size : " << printer.CStrSize() << endl;

    delete doc;
    putchar(10);
    putchar(10);
    cout << "Hello World!" << endl;
    return 0;
}
