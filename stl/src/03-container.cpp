#include <iostream>
#include <vector>

using namespace std;

class A {
public:
    A () {
        cout << "Parameterless constructor!" << this << endl;
    }
    A (int data) {
        data_  = data;
        cout << "Constructor with parameter!" << this << endl;
    }
    ~A () {
        cout << "Destructor!" << this << endl;
    }
    A(const A &another) {
        cout << "Copy constructor!" << this << " = " << &another<< endl;
    }
    A & operator= (const A &another) {
        cout << "Assignment overload function!" << this << " = " << &another << endl;
    }
    void setData(int data) {
        data_ = data;
    }
    int getData(void) {
        return data_;
    }
private:
    int data_;
};

int main() {
    A a(10);
    cout << "a.data = " << a.getData() << endl;
    {
        vector<A> va;
        va.push_back(a);
        va[0].setData(20);
        cout << va[0].getData() << endl;
    }
    cout << "a.data = " << a.getData() << endl;
    cout << "Hello World!" << endl;
    return 0;
}
