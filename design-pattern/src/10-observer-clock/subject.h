#ifndef SUBJECT_H
#define SUBJECT_H


#include <list>
#include "observer.h"

class Subject {
public:
    Subject();
    void registerObserver(Observer *ob);
    void removeObserver(Observer *ob);

    virtual void notify() = 0;
protected:
    std::list<Observer *> lo_;
};

#endif // SUBJECT_H
