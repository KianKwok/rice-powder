#include <stdio.h>
#include <stdlib.h>

typedef struct _TreeNode {
    struct _TreeNode *right_;
    struct _TreeNode *left_;
    int data_;
} TreeNode;

void MidOrder(TreeNode *r);
void insertBst_recursion(TreeNode * * r, int data);
TreeNode * GetParentNode(TreeNode *r, TreeNode *child);
TreeNode * SearchBSTRecursion(TreeNode *r, int key);
TreeNode * GetMinNode(TreeNode *r);

void DeleteBST(TreeNode **r, TreeNode *pDel) {
    if (*r == NULL || pDel == NULL)
        return;
    TreeNode *t = *r;
    TreeNode *parent = GetParentNode(*r, pDel);
    if (pDel->left_ == NULL && pDel->right_ == NULL) {
        if (t == pDel) {
            free(t);
            *r = NULL;
            return;
        }
        if (parent->left_ == pDel)
            parent->left_ = NULL;
        else
            parent->right_ = NULL;
        free(pDel);
    } else if (pDel->left_ != NULL && pDel->right_ == NULL) {
        if (t == pDel) {
            *r = pDel->left_;
            free(pDel);
            return;
        }
        if (parent->left_ == pDel)
            parent->left_ = pDel->left_;
        else
            parent->right_ = pDel->left_;
        free(pDel);
    } else if (pDel->left_ == NULL && pDel->right_ != NULL) {
        if (t == pDel) {
            *r = pDel->right_;
            free(pDel);
            return;
        }
        if (parent->left_ == pDel)
            parent->left_ = pDel->right_;
        else
            parent->right_ = pDel->right_;
        free(pDel);
    } else {
        TreeNode *min_right = GetMinNode(pDel->right_);
        pDel->data_ = min_right->data_;
        TreeNode *min_right_parent = GetParentNode(r, min_right);
        if (min_right_parent->left_ == min_right)
            min_right_parent->left_ = NULL;
        else
            min_right_parent->right_ = NULL;
        free(min_right);
    }

}
int main() {
    TreeNode *root = NULL;
    insertBst_recursion(&root, 30);
    insertBst_recursion(&root, 8);
    insertBst_recursion(&root, 15);
    insertBst_recursion(&root, 36);
    insertBst_recursion(&root, 100);
    insertBst_recursion(&root, 32);
    MidOrder(root);
    putchar(10);

    TreeNode *pFind = SearchBSTRecursion(root, 36);
    DeleteBST(&root, pFind);
    MidOrder(root);
    putchar(10);

    printf("Hello World!\n");
    return 0;
}

void MidOrder(TreeNode *r) {
    if (r) {
        MidOrder(r->left_);
        printf("%d ", r->data_);
        MidOrder(r->right_);
    }
}
void insertBst_recursion(TreeNode * * r, int data) {
    if (*r == NULL) {
        *r = (TreeNode *)malloc(sizeof(TreeNode));
        (*r)->data_ = data;
        (*r)->left_ = NULL;
        (*r)->right_ = NULL;
    } else if (data > (*r)->data_) {
        insertBst_recursion(&(*r)->right_, data);
    } else {
        insertBst_recursion(&(*r)->left_, data);
    }
}
TreeNode * GetParentNode(TreeNode *r, TreeNode *child) {
    static TreeNode *parent = NULL;
    if (r) {
        if (r->left_ == child || r->right_ == child) {
            parent = r;
        }
        GetParentNode(r->left_, child);
        GetParentNode(r->right_,child);
    }
    return parent;
}
TreeNode * SearchBSTRecursion(TreeNode *r, int key) {
    if (r) {
        if (r->data_ == key)
            return r;
        else if (r->data_ > key)
            return SearchBSTRecursion(r->left_, key);
        else
            return SearchBSTRecursion(r->right_, key);
    }
    else
        return NULL;
}
TreeNode * GetMinNode(TreeNode *r) {
    if (r) {
        while (r->left_) {
            r = r->left_;
        }
        return r;
    }
    return NULL;
}
