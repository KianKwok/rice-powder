#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;
using namespace std::placeholders;

class Person {
public:
    Person (const string &name)
        :name_(name) {}
    void print () const {
        cout << name_ << endl;
    }
    void print2 (const string &prefix) const {
        cout << prefix << " " << name_ << endl;
    }

private:
    string name_;
};

int main() {
    vector<Person> vp = {
        Person("aaa"),
        Person("bbb"),
        Person("ccc")
    };

    for_each(vp.begin(), vp.end(), bind(&Person::print, _1));
    putchar(10);

    for_each(vp.begin(), vp.end(), bind(&Person::print2, _1, "Person"));
    putchar(10);

    cout << "Hello World!" << endl;
    return 0;
}
