#include <iostream>
#include <queue>

using namespace std;

int main() {
    queue<char> qc;

    for (char ch = 'a'; ch < 'z'; ++ch) {
        qc.push(ch);
    }

    while (!qc.empty()) {
        cout << qc.front() << " ";
        qc.pop();
    }
    putchar(10);

    cout << "Hello World!" << endl;
    return 0;
}
