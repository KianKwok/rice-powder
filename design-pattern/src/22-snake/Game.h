#ifndef GAME_H
#define GAME_H


#include "Screen.h"
#include "Snake.h"
#include "Food.h"

class Game {
public:
    Game();
    void run();
protected:
    Screen *screan_;
    Snake *snake_;
    Food *food_;
};

#endif // GAME_H
